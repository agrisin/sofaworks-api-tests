pipeline {

    agent any

    options {
        skipDefaultCheckout true
    }

    stages {
        stage('checkout') {
            steps {
                checkout scm
            }
        }
        stage('api tests') {
            steps {
                catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                    script {
                        sh "./gradlew clean test -Dconfig.env=${environment} -Dsuite=${testSuite}"
                    }
                }
            }
        }
        stage('allure report') {
            steps {
                script {
                    allure([
                            includeProperties: false,
                            jdk              : '',
                            properties       : [[key: 'allure.issues.tracker.pattern', value: 'http://tracker.company.com/%s']],
                            reportBuildPolicy: 'ALWAYS',
                            results          : [[path: 'build/allure-results']]
                    ])
                }
            }
        }
    }

    post {
        always {
            publishHTML(target: [
                    allowMissing         : false,
                    alwaysLinkToLastBuild: true,
                    keepAll              : true,
                    reportDir            : 'build/reports/spock',
                    reportFiles          : 'index.html',
                    reportName           : "Test Report"
            ])
        }

        success {
            echo 'pipeline unit tests PASSED'
        }

        failure {
            echo 'pipeline unit tests FAILED'
        }
    }
}