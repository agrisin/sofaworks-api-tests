/*
    Put here all the environment configuration properties
 */

environments
        {
            qa
                    {
                        baseUrl = 'http://qa-develop.y.sofology.co.uk/'
                        securedBaseUrl = 'https://qa-develop.y.sofology.co.uk/'
                        adminUrl = 'http://qa-develop.y.sofology.co.uk:9001/'
                        solrUrl = 'http://qa-develop.y.sofology.co.uk:8983/solr'
                        ssh = 'qa-develop.y.sofology.co.uk'
                        omsUrl = 'https://rabbitmq.qa.sofology.co.uk'
                        logisticsUrl = 'http://logistics.qa.sofology.co.uk/'
                    }

            master
                    {
                        baseUrl = 'http://qa.y.sofology.co.uk/'
                        securedBaseUrl = 'https://qa.y.sofology.co.uk/'
                        adminUrl = 'http://qa.y.sofology.co.uk:9001/'
                        solrUrl = 'http://qa.y.sofology.co.uk:8983/solr'
                        ssh = 'qa.y.sofology.co.uk'
                        omsUrl = 'https://rabbitmq.qa.sofology.co.uk'
                        logisticsUrl = 'http://logistics.qa.sofology.co.uk/'
                    }
        }
