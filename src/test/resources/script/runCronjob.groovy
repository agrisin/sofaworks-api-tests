package script

def service = spring.getBean('cronJobService')
def job = service.getCronJob("$cronjob")
def synchronous = true
service.performCronJob(job, synchronous)

return 'OK'