import data.annotations.Regression
import data.annotations.Slow
import data.annotations.Smoke

runner {
    def suite = System.properties.getProperty("suite")

    if (!System.properties.containsKey('test.include.slow'))
    {
        exclude Slow
    }

    switch (suite)
    {
        case 'smoke':
            include Smoke
            break
        case 'regression':
            include Regression
            break
        default:
            break
    }
}