package apitests

import adminconsole.StockImport
import data.annotations.Smoke
import spock.lang.Specification
import traits.WithAdminApi

import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAdminApi

class AdminTestSuite extends Specification implements WithAdminApi
{
    def setupSpec()
    {
        reportHeader '<h2>Admin tests suite</h2>'
    }

    @Smoke
    def 'All critical cronjobs should be available'()
    {
        when: 'receive all cron jobs available'
        def response = adminApi.getCronJobsList()
        def rootNode = new XmlSlurper().parseText(response.body)
        String cronJobsList = rootNode.cronjob.@code.toString()

        then: 'critical cron jobs exist'
        response.httpStatus == SC_OK
        cronJobsList.contains('dynamicCategoryPopulatorCronJob')
        cronJobsList.contains('pendingCategoryPopulatorTasksPoller')
        cronJobsList.contains('deltaDataExportCronJob')
        cronJobsList.contains('swkCleanupCronJobsPerformable')
        cronJobsList.contains('swkCleanupDataProcessSchedulesPerformable')
        cronJobsList.contains('cleanUpLogsJobPerformable')
        cronJobsList.contains('cmsSofaworksSite-CartRemovalJob')
        cronJobsList.contains('full-swkIndex-cronJob')
        cronJobsList.contains('update-swkIndex-cronJob')
        cronJobsList.contains('blackcardGroupCleanupJob')
        cronJobsList.contains('categoryCurationPartialRebuildJob')
    }

    def 'Stock import should update product current stock available'()
    {
        given: 'stock import file prepared'
        int initialStock = 1
        int updatedStock = 5
        String stockProduct = '615-75830'
        String warehouse = 'WARRINGTON'
        String stockType = 'OVERSTOCK'
        StockImport stockImport = new StockImport(stockProduct, stockType, warehouse)

        when: 'stock import is executed'
        stockImport.execute(initialStock)
        def initialStockAvailable = getAvailableStock(stockProduct, stockType, warehouse)
        stockImport.execute(updatedStock)
        def updatedStockAvailable = getAvailableStock(stockProduct, stockType, warehouse)

        then: 'product stock is updated accordingly'
        initialStockAvailable == initialStock
        updatedStockAvailable == updatedStock
    }

    int getAvailableStock(String productId, String stockType, String warehouse)
    {
        def stockAvailable = adminApi.flexSearch('getStockAvailableByProduct.fxs', [
                productId: productId,
                warehouse: warehouse,
                stockType: stockType
        ])

        Integer.valueOf(stockAvailable)
    }
}
