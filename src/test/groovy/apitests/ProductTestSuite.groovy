package apitests

import java.text.SimpleDateFormat

import data.annotations.Slow
import groovy.json.JsonSlurper
import org.apache.commons.lang3.RandomStringUtils
import spock.lang.Specification
import spock.util.concurrent.PollingConditions
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.CatalogVersions.CATALOG_VERSION_ONLINE
import static data.constants.CatalogVersions.CATALOG_VERSION_STAGED
import static data.constants.Credentials.MERCH_MANAGER
import static data.constants.Credentials.PRICING_MANAGER
import static data.constants.Payloads.PRODUCT_FIELDS_PAYLOAD
import static data.constants.Payloads.PRODUCT_PRICE_PAYLOAD
import static data.constants.Products.OCCASIONAL_PRODUCT
import static data.constants.UserGroups.MERCH_USER_GROUP
import static data.constants.UserGroups.PRICING_USER_GROUP
import static services.ApiClient.getPricingApi
import static services.ApiClient.getProductApi

class ProductTestSuite extends Specification implements WithCustomerApi, WithProductApi
{
    static def jsonSlurper = new JsonSlurper()

    static final String RANGE_NAME = 'TacitKnowledgeRange'

    static String merchAccessToken
    static String pricingAccessToken

    def setupSpec()
    {
        reportHeader '<h2>Product tests suite</h2>'
        addOccasionalProduct(OCCASIONAL_PRODUCT, CATALOG_VERSION_STAGED)
        addOccasionalProduct(OCCASIONAL_PRODUCT, CATALOG_VERSION_ONLINE)
        addEmployee(PRICING_MANAGER, PRICING_USER_GROUP)
        addEmployee(MERCH_MANAGER, MERCH_USER_GROUP)
    }

    def setup()
    {
        pricingAccessToken = login(PRICING_MANAGER)
        merchAccessToken = login(MERCH_MANAGER)
    }

    @Slow
    def 'Product sync should update online version of product from staged'()
    {
        given: 'product update feed prepared'
        String newName = RandomStringUtils.random(12, true, true)
        String productUpdatePayload = "{\"name\":\"${newName}\"}"
        String syncPayload = "{\"products\": [\"${OCCASIONAL_PRODUCT.variant}\"]}"

        when: 'update stage version of product'
        productApi.updateProductInfo(OCCASIONAL_PRODUCT.variant, CATALOG_VERSION_STAGED, productUpdatePayload)

        and: 'initiate product synchronization'
        productApi.syncProduct(syncPayload, merchAccessToken)

        then: 'online version of the product is updated with changes made to staged'
        productName(OCCASIONAL_PRODUCT.variant, CATALOG_VERSION_STAGED) == newName
        PollingConditions conditions = new PollingConditions(timeout: 120, initialDelay: 2, delay: 1)
        conditions.eventually {
            productName(OCCASIONAL_PRODUCT.variant, CATALOG_VERSION_ONLINE) == newName
        }
    }

    def 'User with price manager role should be able to update occasional product price'()
    {
        given: 'occasional product price update feed'
        BigDecimal oldPrice = price(OCCASIONAL_PRODUCT.variant)
        BigDecimal newPrice = oldPrice + 10.0
        String productKey = productKey(OCCASIONAL_PRODUCT.variant)
        String payload = preparePayload(PRODUCT_PRICE_PAYLOAD, [
                productKey: productKey,
                newPrice  : newPrice
        ])

        when: 'price manager updates price value'
        pricingApi.priceUpdate(pricingAccessToken, payload)

        then: 'product price is updated with the new value'
        price(OCCASIONAL_PRODUCT.variant) == newPrice
    }

    def 'User with merch manager role should be able to update occasional product fields'()
    {
        given: 'occasional product fields update feed is prepared'
        Random r = new Random()
        String webDescription = RandomStringUtils.random(12, true, true)
        Map payloadInput = [
                productId     : OCCASIONAL_PRODUCT.variant,
                webDescription: webDescription,
                leadTime      : r.nextInt(100),
                d1            : r.nextInt(10),
                d2            : r.nextInt(10),
                d3            : r.nextInt(10),
                d4            : r.nextInt(10),
                scoopImage    : "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRp0yveRVrI50k3ULybZy1b2d-vXBd6NYv2chIKgCtJICq7GCLK",
                onlineDate    : timestamp()
        ]
        String payload = preparePayload(PRODUCT_FIELDS_PAYLOAD, payloadInput)

        when: 'merch manager updates product fields'
        productApi.occasionalProductsFieldsUpdate(merchAccessToken, payload)
        def response = productApi.getOccasionalProducts(RANGE_NAME, merchAccessToken)
        def product = productFromResponse(response.body)
        def variant = variantFromResponse(response.body)

        then: 'product fields are updated'
        variant.code == payloadInput.productId
        product.d1 == payloadInput.d1
        product.d2 == payloadInput.d2
        product.d3 == payloadInput.d3
        product.d4 == payloadInput.d4
        variant.name == payloadInput.webDescription
        variant.scoopImage == payloadInput.scoopImage
        variant.onlineDate == payloadInput.onlineDate
    }

    String productName(String productId, String catalogVersion)
    {
        def response = productApi.getProductInfo(productId, catalogVersion)
        def root = new XmlSlurper().parseText(response.body)
        root.name
    }

    Map productFromResponse(String response)
    {
        jsonSlurper.parseText(response).products.find { product -> product.variants.find { it.code == OCCASIONAL_PRODUCT.variant } } as Map
    }

    Map variantFromResponse(String response)
    {
        def product = productFromResponse(response)
        product.variants.find { it.code == OCCASIONAL_PRODUCT.variant } as Map
    }

    String productKey(String productId)
    {
        def response = pricingApi.priceForOccasionalRange(RANGE_NAME, merchAccessToken)
        jsonSlurper.parseText(response.body).productVariants.find { it.code == productId }.key
    }

    BigDecimal price(String productId)
    {
        def response = pricingApi.priceForOccasionalRange(RANGE_NAME, pricingAccessToken)
        jsonSlurper.parseText(response.body).productVariants.find { it.code == productId }.prices.retailPrice.value.first()
    }

    String timestamp()
    {
        TimeZone timeZone = TimeZone.getTimeZone('UTC')
        Date timestamp = Calendar.getInstance().getTime()
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+0000")
        formatter.setTimeZone(timeZone)

        formatter.format(timestamp)
    }
}
