package apitests

import java.text.SimpleDateFormat
import java.time.LocalDate

import groovy.json.JsonSlurper
import spock.lang.Ignore
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Products.OCCASIONAL_PRODUCT
import static data.constants.Products.OUTLET_PRODUCT_EX_DISPLAY
import static data.constants.Products.OUTLET_PRODUCT_OVERSTOCK
import static data.constants.Products.OUTLET_PRODUCT_QUICKSTOCK
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_A
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.Products.SOFASHIELD_PRODUCT
import static data.constants.Products.UPHOLSTERY_FOOTSTOOL_PRODUCT
import static data.constants.SuperCategories.EX_DISPLAY_CATEGORY
import static data.constants.SuperCategories.FOOTSTOOL_CATEGORY
import static data.constants.SuperCategories.OVERSTOCK_CATEGORY
import static data.constants.SuperCategories.QUICKSTOCK_CATEGORY
import static data.constants.SuperCategories.REFURBISHED_CATEGORY
import static data.constants.SuperCategories.UPHOLSTERY_CATEGORY
import static org.apache.http.HttpStatus.SC_BAD_REQUEST
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAccountApi
import static services.ApiClient.getAdminApi
import static services.ApiClient.getCartApi

class CartTestSuite extends Specification implements WithProductApi, WithCustomerApi
{
    static def jsonSlurper = new JsonSlurper()

    static String uid
    static String accessToken
    static String cartGuid
    static String addressPayload

    def setupSpec()
    {
        reportHeader '<h2>Cart tests suite</h2>'
        addProduct(SIMPLE_PRODUCT)
        addProduct(UPHOLSTERY_FOOTSTOOL_PRODUCT)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, FOOTSTOOL_CATEGORY)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, UPHOLSTERY_CATEGORY)
        addOccasionalProduct(OCCASIONAL_PRODUCT)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
    }

    def setup()
    {
        cartGuid = cartApi.create(uid, accessToken)
    }

    def 'Customer should be able to create a cart'()
    {
        expect: 'registered cart customer is created'
        !cartGuid.isEmpty()

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to add entry to cart'()
    {
        when: 'customer adds product entries to cart'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)

        then: 'product is added to cart'
        jsonSlurper.parseText(getCart.body).entries.product.code.first() == SIMPLE_PRODUCT.variant

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Cart total should be calculated'()
    {
        when: 'customer adds product entries to cart'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)

        then: 'cart total is calculated'
        totalCartPrice() == SIMPLE_PRODUCT.price

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to remove cart entry'()
    {
        given: 'customer with a cart and product added'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)

        when: 'customer removes product entry'
        cartApi.removeEntry(uid, cartGuid, 0, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)

        then: 'cart does not contain removed product'
        !getCart.body.contains(SIMPLE_PRODUCT.variant)

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to remove cart'()
    {
        when: 'customer removes cart'
        cartApi.removeCart(uid, cartGuid, accessToken)
        def response = cartApi.details(uid, cartGuid, accessToken)

        then: 'cart is removed'
        response.httpStatus == SC_BAD_REQUEST
        jsonSlurper.parseText(response.body).errors.message.first() == 'Cart not found.'
    }

    def 'Customer should be able to add outlet product entry to cart'()
    {
        given: 'outlet product is created'
        addOutletProduct(product, categoryId)

        when: 'customer adds outlet product to cart'
        def response = cartApi.addOutletEntry(uid, cartGuid, product.variant, product.stockType, accessToken)

        then: 'outlet product is added to cart'
        jsonSlurper.parseText(response.body).entry.stockType == cartDetailsStockType

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)

        where: 'different types of outlet products'
        product                      | categoryId           | cartDetailsStockType
        OUTLET_PRODUCT_REFURBISHED_A | REFURBISHED_CATEGORY | 'Refurbished Grade A'
        OUTLET_PRODUCT_EX_DISPLAY    | EX_DISPLAY_CATEGORY  | 'Ex-Display'
        OUTLET_PRODUCT_OVERSTOCK     | OVERSTOCK_CATEGORY   | 'Overstock'
    }

    @Ignore('need to investigate functionality')
    def 'verify that basket price depends to stock amount - #stockType'()
    {
        given:
        BigDecimal qty = 9
        addOutletProduct(product, categoryId)
        addUpdatePrice(product, price)

        when:
        cartApi.addOutletEntry(uid, cartGuid, product.variant, product.stockType, accessToken, qty)
        assert subTotalCartPrice() == (price * qty)
        def response = accountApi.addAddress(uid, accessToken, addressPayload)
        def addressId = jsonSlurper.parseText(response.body).id as String
        cartApi.addDeliveryAddress(uid, cartGuid, addressId, accessToken)

        then:
        subTotalCartPrice() == expectedTotalAfterDelivery

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)

        where: 'different outlet product types'
        product                      | price | expectedTotalAfterDelivery | categoryId
        OUTLET_PRODUCT_QUICKSTOCK    | 100.0 | 15696.0                    | QUICKSTOCK_CATEGORY
        OUTLET_PRODUCT_REFURBISHED_A | 200.0 | 15696.0                    | REFURBISHED_CATEGORY
        OUTLET_PRODUCT_EX_DISPLAY    | 300.0 | 15696.0                    | EX_DISPLAY_CATEGORY
        OUTLET_PRODUCT_OVERSTOCK     | 400.0 | 15696.0                    | OVERSTOCK_CATEGORY
    }

    def 'Customer should be able to add occasional product entry to cart'()
    {
        when: 'customer adds occasional product to cart'
        cartApi.addEntry(uid, cartGuid, OCCASIONAL_PRODUCT.variant, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)

        then: 'occasional product is added to cart'
        jsonSlurper.parseText(getCart.body).entries.product.code.first() == OCCASIONAL_PRODUCT.variant

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to add sofashield product entry to cart'()
    {
        when: 'customer adds upholstery and sofashield products to cart'
        cartApi.addEntry(uid, cartGuid, UPHOLSTERY_FOOTSTOOL_PRODUCT.variant, accessToken)
        cartApi.addEntry(uid, cartGuid, SOFASHIELD_PRODUCT.variant, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)
        def productList = jsonSlurper.parseText(getCart.body).entries.product.code as List

        then: 'upholstery and sofashield products added to cart'
        productList.contains(UPHOLSTERY_FOOTSTOOL_PRODUCT.variant)
        productList.contains(SOFASHIELD_PRODUCT.variant)

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to add addon product entries to cart'()
    {
        given: 'addon products'
        String fabricCareKitAddon = 'fabricCareKit'
        String leatherCareKitAddon = 'leatherCareKit'

        when: 'customer adds addon products to cart'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        cartApi.addEntry(uid, cartGuid, fabricCareKitAddon, accessToken)
        cartApi.addEntry(uid, cartGuid, leatherCareKitAddon, accessToken)
        def addonsRequest = cartApi.getAddonEntries(uid, cartGuid, 'careKits', accessToken)
        def addonsList = jsonSlurper.parseText(addonsRequest.body).products.code as List

        then: 'addon products added to cart'
        addonsList.contains(fabricCareKitAddon)
        addonsList.contains(leatherCareKitAddon)

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Eca date should be able to be applied to cart'()
    {
        given: 'cart with a product is created'
        Date date = new Date()
        String timestamp = unixTimestamp(date)
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)

        when: 'eca date is applied to cart'
        def response = cartApi.applyEcaDate(uid, cartGuid, timestamp, accessToken)
        def ecaDate = adminApi.flexSearch('getEcaDateOnCartByGUID.fxs', [cartGuid: cartGuid])

        then: 'eca date is stored in database'
        response.httpStatus == SC_OK
        dateFormat(date) == LocalDate.parse(ecaDate, 'yyyy-MM-dd hh:mm:ss.s').toString()

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    String unixTimestamp(Date date)
    {
        date.getTime()
    }

    private String dateFormat(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat('yyyy-MM-dd')
        formatter.format(date)
    }

    BigDecimal totalCartPrice()
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).totalPriceWithTax.value
    }

    BigDecimal subTotalCartPrice()
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).subTotal.value
    }

    String accessToken(String response)
    {
        jsonSlurper.parseText(response).access_token
    }
}
