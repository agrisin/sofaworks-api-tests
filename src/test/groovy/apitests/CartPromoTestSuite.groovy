package apitests

import spock.lang.Ignore
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Credentials.ANONYMOUS_USER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Payloads.CART_DISCOUNT_PAYLOAD
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.Products.UPHOLSTERY_FOOTSTOOL_PRODUCT
import static data.constants.Promotions.ANONYMOUS_VOUCHER_CODE
import static data.constants.Promotions.ANONYMOUS_VOUCHER_NAME
import static data.constants.Promotions.ANONYMOUS_VOUCHER_VALUE
import static data.constants.Promotions.DISCOUNT_CODE
import static data.constants.Promotions.DISCOUNT_VALUE
import static data.constants.Promotions.MULTI_VALUE_VOUCHER_CODE
import static data.constants.Promotions.MULTI_VALUE_VOUCHER_VALUE
import static data.constants.Promotions.PROMO_PRODUCT
import static data.constants.SuperCategories.FOOTSTOOL_CATEGORY
import static data.constants.SuperCategories.UPHOLSTERY_CATEGORY
import static services.ApiClient.getAccountApi
import static services.ApiClient.getCartApi
import static services.ApiClient.getPromoApi

class CartPromoTestSuite extends Specification implements WithCustomerApi, WithProductApi
{
    static String uid
    static String accessToken
    static String cartGuid
    static String addressPayload

    def setupSpec()
    {
        reportHeader '<h2>Cart promotions tests suite</h2>'
        addProduct(SIMPLE_PRODUCT)
        addProduct(UPHOLSTERY_FOOTSTOOL_PRODUCT)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, FOOTSTOOL_CATEGORY)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, UPHOLSTERY_CATEGORY)
        addVoucher(ANONYMOUS_VOUCHER_CODE, ANONYMOUS_VOUCHER_NAME, ANONYMOUS_VOUCHER_VALUE)
        addDiscount(DISCOUNT_CODE, DISCOUNT_VALUE)
        addMultivalueSerialVoucher(MULTI_VALUE_VOUCHER_CODE, MULTI_VALUE_VOUCHER_VALUE)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
    }

    def setup()
    {
        cartGuid = cartApi.create(uid, accessToken)
    }

    def cleanup()
    {
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    @Ignore('legacy promotion that is no longer on live')
    def 'Customers with eligible products in cart should have promo item in cart'()
    {
        when: 'customer adds promo eligible product to cart'
        cartApi.addEntry(uid, cartGuid, UPHOLSTERY_FOOTSTOOL_PRODUCT.variant, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)
        def productList = jsonSlurper.parseText(getCart.body).entries.product.code as List

        then: 'promo item is added to cart'
        productList.contains(PROMO_PRODUCT)
    }

    def 'Customers with no eligible products in cart should have no promo item in cart'()
    {
        when: 'customer adds product to cart that is not eligible for promo'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)
        def productList = jsonSlurper.parseText(getCart.body).entries.product.code as List

        then: 'promo item is not added to cart'
        !productList.contains(PROMO_PRODUCT)
    }

    def 'Customer should be able to add multi value serial voucher to cart'()
    {
        given: 'customer has a cart with products added'
        def response = promoApi.generateMultiValueSerialVoucher(MULTI_VALUE_VOUCHER_CODE, uid)
        def voucherCode = jsonSlurper.parseText(response.body).voucherCode as String
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        def cartTotalBefore = totalCartPrice()

        when: 'voucher is applied to cart'
        cartApi.applyVoucher(uid, cartGuid, voucherCode, accessToken)
        def cartTotalAfter = totalCartPrice()

        then: 'voucher is applied and cart total is reduced'
        (cartTotalBefore - cartTotalAfter) == MULTI_VALUE_VOUCHER_VALUE
    }

    def 'Anonymous customer should be able to add voucher to cart'()
    {
        given: 'anonymous customer cart is created'
        String anonymousCartGuid = cartApi.createAnonymous(ANONYMOUS_USER)
        cartApi.addEntryAnonymous(ANONYMOUS_USER, anonymousCartGuid, SIMPLE_PRODUCT.variant)
        def getCart = cartApi.detailsAnonymous(ANONYMOUS_USER, anonymousCartGuid)
        BigDecimal totalValueBefore = jsonSlurper.parseText(getCart.body).totalPriceWithTax.value

        when: 'voucher is applied to anonymous cart'
        cartApi.applyVoucherAnonymous(ANONYMOUS_USER, anonymousCartGuid, ANONYMOUS_VOUCHER_CODE)
        getCart = cartApi.detailsAnonymous(ANONYMOUS_USER, anonymousCartGuid)
        BigDecimal totalValueAfter = jsonSlurper.parseText(getCart.body).totalPriceWithTax.value

        then: 'voucher is applied and cart total is reduced'
        (totalValueBefore - totalValueAfter) == ANONYMOUS_VOUCHER_VALUE
    }

    def 'Customer should be able to apply one time discount to cart'()
    {
        given: 'customer cart is created'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        def addAddressResponse = accountApi.addAddress(uid, accessToken, addressPayload)
        def addressId = jsonSlurper.parseText(addAddressResponse.body).id as String
        cartApi.addDeliveryAddress(uid, cartGuid, addressId, accessToken)
        cartApi.setDeliveryOption(uid, cartGuid, DELIVERY_OPTION_VAN, accessToken)
        cartApi.confirmReservation(uid, cartGuid, accessToken)

        when: 'one time discount is applied to cart'
        String payload = preparePayload(CART_DISCOUNT_PAYLOAD, [
                discountCode : DISCOUNT_CODE,
                discountValue: DISCOUNT_VALUE
        ])
        def adminAccessToken = login(SOFOLOGIST_USER)
        cartApi.applyOnetimeDiscount(uid, cartGuid, adminAccessToken, payload)

        then: 'one time discount is applied and cart total is reduced'
        discountApplied() == DISCOUNT_VALUE
    }

    BigDecimal discountApplied()
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).orderDiscounts.value
    }

    BigDecimal totalCartPrice()
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).totalPriceWithTax.value
    }
}
