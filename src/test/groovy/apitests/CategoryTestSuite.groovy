package apitests

import data.annotations.Slow
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Categories.OCCASIONAL_VARIANT_CATEGORY
import static data.constants.Credentials.MERCH_MANAGER
import static data.constants.Products.OUTLET_PRODUCT_EX_DISPLAY
import static data.constants.Products.OUTLET_PRODUCT_OVERSTOCK
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_A
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAdminApi
import static services.ApiClient.getCategoryApi

class CategoryTestSuite extends Specification implements WithCustomerApi, WithProductApi
{
    static final String PRODUCT_ID = 'SKU000012000'
    static final String CATEGORY_ID = 'AutomationTest'

    def setupSpec()
    {
        reportHeader '<h2>Category tests suite</h2>'
    }

    def 'Category products should be returned'()
    {
        when: 'search category products'
        def response = categoryApi.search(categoryName)

        then: 'category products are returned'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).pagination.totalResults.toInteger() > 0

        where: 'different categories'
        categoryName  | _
        'Leather'     | _
        'ExDisplay'   | _
        'Refurbished' | _
    }

    def 'Outlet category products should be returned'()
    {
        when: 'search ourltet category products'
        addOutletProduct(product, categoryId)
        def response = categoryApi.outlet(categoryId)

        then: 'outlet category products are returned'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).pagination.totalResults.toInteger() > 0

        where: 'different outlet categories'
        product                      | categoryId
        OUTLET_PRODUCT_OVERSTOCK     | 'Overstock'
        OUTLET_PRODUCT_EX_DISPLAY    | 'ExDisplay'
        OUTLET_PRODUCT_REFURBISHED_A | 'Refurbished'
    }

    @Slow
    def 'Categories related to a super category should be returned'()
    {
        given: 'super category'
        String category = 'Home'

        when: 'search for categories related to a super category'
        def response = categoryApi.superCategory(category)
        def subCategories = jsonSlurper.parseText(response.body).subcategories.id as List

        then: 'categories are returned'
        response.httpStatus == SC_OK
        subCategories.contains('Fabric')
        subCategories.contains('Leather')
        subCategories.contains('Recliners')
        subCategories.contains('SofaBeds')
        subCategories.contains('ExpressSofas')
        subCategories.contains('Corners')
    }

    def 'Occasional variant value categories should be returned'()
    {
        given: 'occasional value category created'
        addCategory(OCCASIONAL_VARIANT_CATEGORY)
        String accessToken = login(MERCH_MANAGER)

        when: 'search occasional categories'
        def response = categoryApi.occasional(accessToken)
        def categoriesList = jsonSlurper.parseText(response.body).categories.code as List

        then: 'created occasional value category is returned'
        categoriesList.contains(OCCASIONAL_VARIANT_CATEGORY.variantValue)
    }

    def 'Swatches occasional variants categories should be returned'()
    {
        when: 'search swatch categories'
        def response = categoryApi.swatches()
        def swatchesList = jsonSlurper.parseText(response.body).swatches.code as List

        then: 'swatch categories should be returned'
        swatchesList.size() > 0
    }

    def 'User with merch manager role should be able to add products to category'()
    {
        given: 'user logged in as merch manager'
        String version = 'Staged'
        addSuperCategory(CATEGORY_ID, version)
        String accessToken = login(MERCH_MANAGER)

        when: 'user adds product to category'
        categoryApi.addProductToCategory(PRODUCT_ID, CATEGORY_ID, accessToken)
        def pk = productInCategory(PRODUCT_ID, CATEGORY_ID, version)

        then: 'product is related to category in database'
        pk != ''

        cleanup:
        adminApi.impexImport('removeProductCategoryRelationship.impex', [pk: pk])
    }
}
