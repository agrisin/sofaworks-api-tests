package apitests

import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithOrderApi
import traits.WithProductApi

import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.FinanceTypes.FINANCE_TYPE_BNPL
import static data.constants.FinanceTypes.FINANCE_TYPE_IFC
import static data.constants.Products.SIMPLE_PRODUCT
import static services.ApiClient.getCartApi
import static services.ApiClient.getFinanceApi
import static services.ApiClient.getOmsApi

class FinanceTestSuite extends Specification implements WithCustomerApi, WithProductApi, WithOrderApi
{
    static String uid
    static String accessToken
    static String cartGuid

    def setupSpec()
    {
        reportHeader '<h2>OMS Finance application tests suite</h2>'
        addCustomer(SOFOLOGIST_USER)
        addProduct(SIMPLE_PRODUCT)
        uid = register()
        accessToken = login(uid)
    }

    def setup()
    {
        cartGuid = createCartWithProduct(SIMPLE_PRODUCT.variant, uid, accessToken)
    }

    def 'Customer should be able to apply BNPL type finance to cart'()
    {
        when: 'customer applies BNPL finance to cart'
        applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_BNPL)
        def getFinanceResponse = financeApi.getFinance(uid, cartGuid, accessToken)
        Map financeMessage = waitAndReturnFinanceMessageByCartGuid(cartGuid)

        then: 'finance application is sent to oms'
        jsonSlurper.parseText(getFinanceResponse.body).id == cartGuid
        financeMessage.Message.Id == cartGuid
        financeMessage.Message.FinanceType == FINANCE_TYPE_BNPL

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to apply IFC type finance to cart'()
    {
        when: 'customer applies IFC finance to cart'
        applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_IFC)
        def getFinanceResponse = financeApi.getFinance(uid, cartGuid, accessToken)
        Map financeMessage = waitAndReturnFinanceMessageByCartGuid(cartGuid)

        then: 'finance application is sent to oms'
        jsonSlurper.parseText(getFinanceResponse.body).id == cartGuid
        financeMessage.Message.Id == cartGuid
        financeMessage.Message.FinanceType == FINANCE_TYPE_IFC

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be allowed to save and not apply partial finance, verify order not sent to oms'()
    {
        given: 'OMS submit value is false'
        String submitValue = 'false'

        when: 'customer appplies finance without submitting'
        applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_BNPL, 0.0, submitValue)

        then: 'finance application is not sent to oms'
        financeMessage(cartGuid).isEmpty()
    }

    def 'Customer is able to apply and publish finance'()
    {
        given: 'BNPL type finance is applied to cart '
        applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_BNPL)
        def getFinanceResponse = financeApi.getFinance(uid, cartGuid, accessToken)

        when: 'finance application is published to oms'
        omsApi.publish(cartGuid)
        def sofoAccessToken = login(SOFOLOGIST_USER)
        def responseFromFinanceProvider = financeApi.getFromFinanceProvider(SOFOLOGIST_USER, cartGuid, sofoAccessToken)

        then: 'customer manager can see finance application from finance provider'
        getFinanceResponse.body.contains(FINANCE_TYPE_BNPL)
        responseFromFinanceProvider.body.contains(cartGuid)

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }
}
