package apitests

import java.text.SimpleDateFormat

import groovy.json.JsonSlurper
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Credentials.PRICING_MANAGER
import static data.constants.Products.BUNDLE_OUTLET_PRODUCT
import static data.constants.Products.OUTLET_PRODUCT_EX_DISPLAY
import static data.constants.Products.OUTLET_PRODUCT_OVERSTOCK
import static data.constants.Products.OUTLET_PRODUCT_QUICKSTOCK
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_A
import static data.constants.SuperCategories.EX_DISPLAY_CATEGORY
import static data.constants.SuperCategories.OVERSTOCK_CATEGORY
import static data.constants.SuperCategories.QUICKSTOCK_CATEGORY
import static data.constants.SuperCategories.REFURBISHED_CATEGORY
import static data.constants.UserGroups.PRICING_USER_GROUP
import static services.ApiClient.getCartApi
import static services.ApiClient.getPricingApi

class PriceTestSuite extends Specification implements WithProductApi, WithCustomerApi
{
    static def jsonSlurper = new JsonSlurper()
    static final String RANGE_NAME = 'WASHINGTON'
    static String uid
    static String accessToken
    static String cartGuid

    def setupSpec()
    {
        reportHeader '<h2>Price tests suite</h2>'
        addEmployee(PRICING_MANAGER, PRICING_USER_GROUP)
        uid = register()
        accessToken = login(uid)
    }

    def setup()
    {
        cartGuid = cartApi.create(uid, accessToken)
    }

    def cleanup()
    {
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'User with price manage role should be able to get price export'()
    {
        given: 'user logged in as price manager'
        def managerAccessToken = login(PRICING_MANAGER)
        def stockType = 'REFURBISHED_GRADE_A'
        def cartDetailsStockType = 'Refurbished Grade A'

        when: 'request price for specific range'
        def response = pricingApi.priceForRange(RANGE_NAME, managerAccessToken)
        def productVariants = jsonSlurper.parseText(response.body).productVariants
        def deletionTime = (productVariants.deletionTime - null).first()

        then: 'product variants with associated price value, currency and category are returned'
        productVariants.pricingCategory.name.first() != null
        productVariants.retailPrice.currency.first() != null
        productVariants.retailPrice.value.first() != null
        deletionTime != null
    }

    def 'Customer should see was price of outlet products in cart - #stockType'()
    {
        given: 'outlet product is added'
        addOutletProduct(product, categoryId)

        when: 'customer adds outlet product to cart'
        cartApi.addOutletEntry(uid, cartGuid, product.variant, product.stockType, accessToken)
        def cartDetails = cartApi.details(uid, cartGuid, accessToken)

        then: 'cart details has was price indicated'
        wasPrice(cartDetails.body, cartDetailsStockType) == product.price

        where: 'different outlet product types'
        product                      | cartDetailsStockType  | categoryId
        OUTLET_PRODUCT_OVERSTOCK     | 'Overstock'           | OVERSTOCK_CATEGORY
        OUTLET_PRODUCT_QUICKSTOCK    | 'Quick Stock'         | QUICKSTOCK_CATEGORY
        OUTLET_PRODUCT_EX_DISPLAY    | 'Ex-Display'          | EX_DISPLAY_CATEGORY
        OUTLET_PRODUCT_REFURBISHED_A | 'Refurbished Grade A' | REFURBISHED_CATEGORY
    }

    def 'Customer should see was price of bundle products in cart'()
    {
        given: 'bundle product is added'
        addOutletProduct(BUNDLE_OUTLET_PRODUCT, EX_DISPLAY_CATEGORY)
        List consistOfProducts = findSuitableProducts(2)
        addProductReferences(BUNDLE_OUTLET_PRODUCT.variant, consistOfProducts[0], consistOfProducts[1])

        when: 'customer adds bundle product to cart'
        cartApi.addOutletEntry(uid, cartGuid, BUNDLE_OUTLET_PRODUCT.variant, BUNDLE_OUTLET_PRODUCT.stockType, accessToken)
        def getCart = cartApi.details(uid, cartGuid, accessToken)

        then: 'cart details has was price indicated'
        wasPrice(getCart.body, 'Ex-Display') == BUNDLE_OUTLET_PRODUCT.price

        cleanup:
        removeProductReferences(BUNDLE_OUTLET_PRODUCT.variant)
    }

    String timestamp()
    {
        Date timestamp = Calendar.getInstance().getTime()
        SimpleDateFormat formatter = new SimpleDateFormat('yyyyMMddhhmmss')

        formatter.format(timestamp)
    }

    BigDecimal wasPrice(String response, String stockType)
    {
        jsonSlurper.parseText(response).entries.find { it.stockType == stockType }.wasPrice.value
    }
}
