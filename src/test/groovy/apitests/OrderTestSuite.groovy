package apitests

import java.text.SimpleDateFormat

import groovy.json.JsonSlurper
import spock.lang.Specification
import ssh.SSHCommandExecute
import traits.WithCustomerApi
import traits.WithOrderApi
import traits.WithProductApi

import static data.constants.Channels.CHANNEL_DESKTOP
import static data.constants.Channels.CHANNEL_IN_STORE
import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.OrderStatuses.COMPLETED
import static data.constants.OrderStatuses.INSUFFICIENT_DEPOSIT
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Payloads.MULTI_PAYMENT_PAYLOAD
import static data.constants.Payloads.ORDER_PAYMENT_PAYLOAD
import static data.constants.Products.SECOND_PRODUCT
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static services.ApiClient.getCartApi
import static services.ApiClient.getOrderApi

class OrderTestSuite extends Specification implements WithCustomerApi, WithProductApi, WithOrderApi
{
    static def jsonSlurper = new JsonSlurper()

    static String uid
    static String accessToken
    static String cartGuid
    static String addressId
    static String addressPayload

    static SSHCommandExecute sshCommandExecute = new SSHCommandExecute()

    def setupSpec()
    {
        reportHeader '<h2>Order tests suite</h2>'
        addCustomer(SOFOLOGIST_USER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
        addProduct(SIMPLE_PRODUCT)
        addProduct(SECOND_PRODUCT)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
        addressId = addAddress(uid, accessToken, addressPayload)
    }

    def setup()
    {
        cartGuid = createAndConfirmCart(SIMPLE_PRODUCT.variant, uid, accessToken, DELIVERY_OPTION_VAN)
    }

    def 'Customer should be able to place an order'()
    {
        given: 'customer has cart with product entries'
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: cartTotal])

        when: 'customer places order with full payment amount'
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code

        then: 'order is placed successfully'
        checkOrderStatus(orderCode, COMPLETED)
    }

    def 'User with customer manager role should be able to amend order - update quantity'()
    {
        given: 'customer placed an order'
        int newQuantity = 2
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: cartTotal])
        orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)

        when: 'customer manager initiate order amendment'
        def managerAccessToken = login(CUSTOMER_MANAGER)
        def amendCartResponse = orderApi.amend(CUSTOMER_MANAGER, cartGuid, managerAccessToken)
        String amendCartGuid = jsonSlurper.parseText(amendCartResponse.body).guid
        def totalPriceBefore = totalCartPrice(uid, amendCartGuid, accessToken)

        and: 'customer changes cart entry quantity'
        cartApi.changeEntryQuantity(uid, amendCartGuid, newQuantity, accessToken)
        def totalPriceAfter = totalCartPrice(uid, amendCartGuid, accessToken)

        then: 'cart entry quantity is updated with new value'
        itemQuantity(uid, amendCartGuid, accessToken) == newQuantity
        totalPriceAfter > totalPriceBefore
    }

    def 'Order should be exported via smart focus order export'()
    {
        given: 'order export path and filename'
        String fileNameAndPath = '/opt/hybris/shared/data/dataexport/orderexportfeed_'
        String files = "${fileNameAndPath}${timestamp('yyyyMMdd')}*"

        when: 'order is placed'
        String orderCode = placeOrder(uid, cartGuid, accessToken)

        then: 'order export feed is found in location specified'
        conditions.eventually {
            assert orderExportFeedFound(files, orderCode): "Order feed not found for order - $orderCode"
        }
    }

    def 'User with customer manager role should be able to amend order - new entries'()
    {
        given: 'customer placed an order'
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: cartTotal])
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code
        checkOrderStatus(orderCode, COMPLETED)

        when: 'customer manager initiate order amendment'
        def managerAccessToken = login(CUSTOMER_MANAGER)
        def amendCartResponse = orderApi.amend(CUSTOMER_MANAGER, cartGuid, managerAccessToken)
        String amendCartGuid = jsonSlurper.parseText(amendCartResponse.body).guid

        and: 'customer adds new product entry to amended cart'
        cartApi.addEntry(uid, amendCartGuid, SECOND_PRODUCT.variant, accessToken)
        def newCartTotal = totalCartPrice(uid, amendCartGuid, accessToken)
        assert newCartTotal > cartTotal
        cartApi.addDeliveryAddress(uid, amendCartGuid, addressId, accessToken)
        cartApi.setDeliveryOption(uid, amendCartGuid, DELIVERY_OPTION_VAN, accessToken)
        cartApi.confirmReservation(uid, amendCartGuid, accessToken)
        paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: newCartTotal])

        and: 'customer places amended order'
        orderResponse = orderApi.place(uid, amendCartGuid, CHANNEL_IN_STORE, accessToken, paymentPayload)
        orderCode = jsonSlurper.parseText(orderResponse.body).code

        then: 'order is placed successfully'
        checkOrderStatus(orderCode, COMPLETED)
    }

    def 'Customer should be able to place an order with multiple payment methods'()
    {
        given: 'customer has cart with product entries'
        def managerAccessToken = login(CUSTOMER_MANAGER)
        String paymentPayload = preparePayload(MULTI_PAYMENT_PAYLOAD)

        when: 'customer places order indicating multiple payment methods'
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_IN_STORE, managerAccessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code

        then: 'order is placed successfully'
        checkOrderStatus(orderCode, COMPLETED)
    }

    def 'insufficient deposit order'()
    {
        given: 'insufficient amount payment payload'
        def insufficientAmount = 1.5
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: insufficientAmount])

        when: 'order is placed with insufficient money amount'
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code

        then: 'order is rejected with status INSUFFICIENT_DEPOSIT'
        checkOrderStatus(orderCode, INSUFFICIENT_DEPOSIT)
    }

    boolean orderExportFeedFound(String fileName, String orderCode)
    {
        String command = "grep $orderCode $fileName"
        sshCommandExecute.execute(command) != null
    }

    private String timestamp(String format = 'yyyyMMddhhmmss')
    {
        TimeZone timeZone = TimeZone.getTimeZone('UTC')
        Date timestampCalendar = Calendar.getInstance().getTime()
        SimpleDateFormat formatter = new SimpleDateFormat(format)
        formatter.setTimeZone(timeZone)

        formatter.format(timestampCalendar)
    }
}
