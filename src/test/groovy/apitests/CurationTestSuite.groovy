package apitests

import spock.lang.Specification
import traits.WithCustomerApi

import static data.constants.Credentials.MERCH_MANAGER
import static data.constants.Payloads.CURATION_LIST_PAYLOAD
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getCurationApi

class CurationTestSuite extends Specification implements WithCustomerApi
{
    static final String categoryId = 'Leather'
    static String curationListPayload

    def setupSpec()
    {
        reportHeader '<h2>Curation list tests suite</h2>'
        curationListPayload = preparePayload(CURATION_LIST_PAYLOAD)
    }

    def 'User with merch manager role should be able to retrieve curation list'()
    {
        given: 'user logged in as merch manager'
        String accessToken = login(MERCH_MANAGER)
        curationApi.putList(categoryId, accessToken, curationListPayload)

        when: 'user requests curation list'
        def curationList = curationApi.getList(categoryId, accessToken)
        def curationListCodes = jsonSlurper.parseText(curationList.body).variants.code as List

        then: 'curation lists are returned'
        curationList.httpStatus == SC_OK
        curationListCodes.size() > 0
    }

    def 'User with merch manager role should be able to delete curation list'()
    {
        given: 'user logged in as merch manager'
        String accessToken = login(MERCH_MANAGER)
        curationApi.putList(categoryId, accessToken, curationListPayload)

        when: 'user attempts to delete curation list'
        curationApi.deleteList(categoryId, accessToken)
        def curationList = curationApi.getList(categoryId, accessToken)

        then: 'curation list is deleted'
        curationList.httpStatus == SC_OK
        jsonSlurper.parseText(curationList.body) == [:]
    }
}
