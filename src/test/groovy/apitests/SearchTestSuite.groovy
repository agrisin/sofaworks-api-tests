package apitests

import data.annotations.Smoke
import groovy.json.JsonSlurper
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAdminApi
import static services.ApiClient.getCategoryApi
import static services.ApiClient.getQuickSearchApi
import static services.ApiClient.getSearchApi
import static services.ApiClient.getSolrApi

class SearchTestSuite extends Specification implements WithCustomerApi, WithProductApi
{
    static def jsonSlurper = new JsonSlurper()

    final static String POSTAL_CODE = 'SW7'
    final static String PHONE_NUMBER = '1234567890'

    def setupSpec()
    {
        reportHeader '<h2>Search tests suite</h2>'
        addCustomer(SOFOLOGIST_USER)
        addCustomerAddress(SOFOLOGIST_USER, POSTAL_CODE, PHONE_NUMBER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
    }

    @Smoke
    def 'User should be able to perform search request'()
    {
        when: 'perform search request'
        def response = searchApi.search()

        then: 'search results are returned'
        response.httpStatus == SC_OK
    }

    def 'User should be able to perform search request by term'()
    {
        when: 'perform search request by term'
        def response = searchApi.search(searchTerm)

        then: 'search results are returned'
        response.httpStatus == SC_OK
        response.body.toLowerCase().contains(searchTerm)

        where: 'different search terms'
        searchTerm                | _
        'clinton'                 | _
        'red'                     | _
        'leather'                 | _
        'corner'                  | _
        'sofa'                    | _
        'red%20leather%20sofabed' | _
    }

    def 'User should be able to perform search outlet product by postal code'()
    {
        given: 'specific range and outlet category'
        String rangeName = 'AKIRA'
        String outletCategory = 'Refurbished'

        when: 'perform search outlet product by postal code request'
        def response = categoryApi.outletProducts(rangeName, postalCode, outletCategory)

        then: 'search results are returned'
        response.httpStatus == SC_OK

        where: 'different postal codes'
        postalCode | _
        'SW7'      | _
        'CT3'      | _
    }

    def 'User should be able to perform quick search request'()
    {
        when: 'perform a quick search request'
        def response = quickSearchApi.search()
        def items = jsonSlurper.parseText(response.body).results

        then: 'search results are returned'
        response.httpStatus == SC_OK
        items.name.contains('AKIRA')
        items.range.contains('AKIRA')
    }

    def 'User should be able to perform solr search request'()
    {
        given: 'product indexed in solr'
        def productId = adminApi.flexSearch('solrIndexedVariant.fxs')

        when: 'perform solr search by product id'
        def response = solrApi.search(productId)

        then: 'product with indexed details is returned'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).response.docs.code_text.contains(productId)
    }

    def 'User should be able to perform search users'()
    {
        given: 'login user with customer manager role'
        String accessToken = login(SOFOLOGIST_USER)

        when: 'perform user search by specific query'
        def response = searchApi.searchUser(query, accessToken)
        def usersList = jsonSlurper.parseText(response.body).users.uid as List

        then: 'search results are returned with user listed¬'
        response.httpStatus == SC_OK
        usersList.contains(SOFOLOGIST_USER)

        where: 'different search queries'
        query           | _
        SOFOLOGIST_USER | _
        POSTAL_CODE     | _
        PHONE_NUMBER    | _
    }

    String accessToken(String response)
    {
        jsonSlurper.parseText(response).access_token
    }
}
