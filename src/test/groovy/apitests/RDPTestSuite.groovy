package apitests

import groovy.json.JsonSlurper
import spock.lang.Specification

import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAdminApi
import static services.ApiClient.getRdpApi
import static services.ApiClient.getSolrApi

class RDPTestSuite extends Specification
{
    static def jsonSlurper = new JsonSlurper()

    final static String RDP_RANGE_NAME = 'AKIRA'

    def setupSpec()
    {
        reportHeader '<h2>RDP tests suite</h2>'
    }

    def 'RDP api should return range and product details'()
    {
        when: 'perform rdp search by specific range'
        def response = rdpApi.search(RDP_RANGE_NAME)

        then: 'range details and products are returned'
        response.httpStatus == SC_OK
        !jsonSlurper.parseText(response.body).results.colours.materialQualityTypes.swkVariants.productTypes.products.name.isEmpty()
    }

    def 'RDP api response should include video urls'()
    {
        given: 'supplier range with video media'
        String rangeWithMedia = rangeWithMedia()

        when: 'perform rdp search by specific range'
        def response = rdpApi.search(rangeWithMedia)
        def results = jsonSlurper.parseText(response.body).results

        then: 'range details with video urls are returned'
        response.httpStatus == SC_OK
        results.videoSpecial[0] =~ 'http://'
        results.videoTextures[0] =~ 'http://'
        results.video360[0] =~ 'http://'
        results.videoFeatures[0] =~ 'http://'
        results.videoOverhead[0] =~ 'http://'
        results.videoSitTest[0] =~ 'http://'
    }

    def 'RDP product api should return response product grouping'()
    {
        when: 'perform rdp product search by specific range'
        def response = rdpApi.searchProducts(RDP_RANGE_NAME)

        then: 'returned products are grouped by query'
        response.httpStatus == SC_OK
        facetQuery(response.body, facetName) == queryValue(RDP_RANGE_NAME, facetName)

        where: 'different facet names'
        facetName   | _
        'Sofas'     | _
        'Chairs'    | _
        'FootStool' | _
        'Corners'   | _
    }

    String accessToken(String response)
    {
        jsonSlurper.parseText(response).access_token
    }

    String facetQuery(String response, String facetName)
    {
        def values = jsonSlurper.parseText(response).facets.values[0]
        values.find { it.name == facetName }.query.query.value
    }

    String queryValue(String rangeName, String facetName)
    {
        "::range:${rangeName}:outletOnly:false:configuration:${facetName}"
    }

    String rangeWithMedia()
    {
        String query = 'id:swkProductCatalog/Online/*'
        String filterQuery = 'outletOnly_boolean:false'
        def response = solrApi.searchRange(query, filterQuery)
        def resultList = jsonSlurper.parseText(response.body).response.docs.id
        String formattedList = "'" + resultList.collect {
            it.replace('swkProductCatalog/Online/', '')
        }.join("', '") + "'"
        String result = adminApi.flexSearch('supplierRangeWithMedia.fxs', [rangeList: formattedList])
        assert result != '': 'range with media not founds'
        result.replace(' ', '%20')
    }
}