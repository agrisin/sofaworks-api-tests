package apitests

import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithOrderApi
import traits.WithProductApi

import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.FinanceTypes.FINANCE_TYPE_BNPL
import static data.constants.FinanceTypes.FINANCE_TYPE_IFC
import static data.constants.OrderStatuses.COMPLETED
import static data.constants.OrderStatuses.FINANCE_DECLINED
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static services.ApiClient.getOmsApi

class OrderFinanceTestSuite extends Specification implements WithCustomerApi, WithProductApi, WithOrderApi
{
    static String uid
    static String accessToken
    static String cartGuid
    static String addressId
    static String addressPayload

    def setupSpec()
    {
        reportHeader '<h2>OMS Order sales tests suite</h2>'
        addCustomer(SOFOLOGIST_USER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
        addProduct(SIMPLE_PRODUCT)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
        addressId = addAddress(uid, accessToken, addressPayload)
    }

    def setup()
    {
        cartGuid = createAndConfirmCart(SIMPLE_PRODUCT.variant, uid, accessToken, DELIVERY_OPTION_VAN)
    }

    def 'Customer should be able to place order with finance type BNPL'()
    {
        when: 'customer places order with finance type BNPL'
        String orderCode = placeOrderWithFinanceApplied(uid, cartGuid, accessToken, FINANCE_TYPE_BNPL)
        Map financeMessage = waitAndReturnFinanceMessageByCartGuid(cartGuid)
        Map salesMessage = waitAndReturnSalesMessageByCartGuid(cartGuid)

        then: 'order is placed successfully with finance application and sales order available in oms'
        checkOrderStatus(orderCode, COMPLETED)
        financeMessage.Message.Id == cartGuid
        financeMessage.Message.FinanceType == FINANCE_TYPE_BNPL
        salesMessage.OrderId == orderCode
    }

    def 'Customer should be able to place order with finance type IFC'()
    {
        when: 'customer places order with finance type IFC'
        String orderCode = placeOrderWithFinanceApplied(uid, cartGuid, accessToken, FINANCE_TYPE_IFC)
        Map financeMessage = waitAndReturnFinanceMessageByCartGuid(cartGuid)
        Map salesMessage = waitAndReturnSalesMessageByCartGuid(cartGuid)

        then: 'order is placed successfully with finance application and sales order available in oms'
        checkOrderStatus(orderCode, COMPLETED)
        financeMessage.Message.Id == cartGuid
        financeMessage.Message.FinanceType == FINANCE_TYPE_IFC
        salesMessage.OrderId == orderCode
    }

    def 'Customer should be able to place order with finance type IFC and zero deposit in finance application'()
    {
        given: 'Finance application status 8-Completed'
        int statusCode = 8
        String statusDescription = 'Completed'

        when: 'customer places order with finance type IFC and zero deposit'
        String orderCode = applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_IFC, 0.0)

        and: 'update finance application with status 9-Declined'
        omsApi.update(cartGuid, statusCode, statusDescription)

        then: 'order is placed successfully'
        checkOrderStatus(orderCode, COMPLETED)
    }

    def 'Order should be rejected when IFC finance is declined'()
    {
        given: 'Finance application status 9-Declined'
        int statusCode = 9
        String statusDescription = 'Declined'

        when: 'customer places order with finance type IFC and zero deposit'
        String orderCode = applyFinance(uid, cartGuid, accessToken, FINANCE_TYPE_IFC, 0.0)

        and: 'update finance application with status 9-Declined'
        omsApi.update(cartGuid, statusCode, statusDescription)

        then: 'order is rejected with status FINANCE_DECLINED'
        checkOrderStatus(orderCode, FINANCE_DECLINED)
    }

    def 'Order should be rejected when BNPL low deposit finance is declined'()
    {
        given: 'Finance application status 9-Declined'
        int statusCode = 9
        String statusDescription = 'Declined'

        when: 'customer places order with finance type BMPL and low deposit'
        String orderCode = placeOrderWithDeposit(uid, cartGuid, accessToken, FINANCE_TYPE_BNPL, 1.0)

        and: 'update finance application with status 9-Declined'
        omsApi.update(cartGuid, statusCode, statusDescription)

        then: 'order is rejected with status FINANCE_DECLINED'
        checkOrderStatus(orderCode, FINANCE_DECLINED)
    }
}
