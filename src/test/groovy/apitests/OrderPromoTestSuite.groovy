package apitests

import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithOrderApi
import traits.WithProductApi

import static data.constants.Channels.CHANNEL_DESKTOP
import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Payloads.CART_DISCOUNT_PAYLOAD
import static data.constants.Payloads.ORDER_PAYMENT_PAYLOAD
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.Promotions.ANONYMOUS_VOUCHER_CODE
import static data.constants.Promotions.ANONYMOUS_VOUCHER_NAME
import static data.constants.Promotions.ANONYMOUS_VOUCHER_VALUE
import static data.constants.Promotions.DISCOUNT_CODE
import static data.constants.Promotions.DISCOUNT_VALUE
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static services.ApiClient.getCartApi
import static services.ApiClient.getOrderApi

class OrderPromoTestSuite extends Specification implements WithCustomerApi, WithProductApi, WithOrderApi
{
    static String uid
    static String accessToken
    static String cartGuid
    static String addressId
    static String addressPayload

    def setupSpec()
    {
        reportHeader '<h2>Order promotions tests suite</h2>'
        addCustomer(SOFOLOGIST_USER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
        addProduct(SIMPLE_PRODUCT)
        addVoucher(ANONYMOUS_VOUCHER_CODE, ANONYMOUS_VOUCHER_NAME, ANONYMOUS_VOUCHER_VALUE)
        addDiscount(DISCOUNT_CODE, DISCOUNT_VALUE)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
        addressId = addAddress(uid, accessToken, addressPayload)
    }

    def setup()
    {
        cartGuid = createAndConfirmCart(SIMPLE_PRODUCT.variant, uid, accessToken, DELIVERY_OPTION_VAN)
    }

    def 'User with customer manager role should be able to apply discount to an order'()
    {
        given: 'discount code and value'
        String payload = preparePayload(CART_DISCOUNT_PAYLOAD, [
                discountCode : DISCOUNT_CODE,
                discountValue: DISCOUNT_VALUE
        ])
        def adminAccessToken = login(SOFOLOGIST_USER)

        when: 'customer manager applies discount to a cart'
        cartApi.applyOnetimeDiscount(uid, cartGuid, adminAccessToken, payload)
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: cartTotal])

        and: 'customer places an order'
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code
        Map salesMessage = waitAndReturnSalesMessageByCartGuid(cartGuid)

        then: 'discount is applied to order and totals are adjusted accordingly'
        salesMessage.OrderId == orderCode
        salesMessage.Discounts[0].Code == DISCOUNT_CODE
        salesMessage.Discounts[0].Value == DISCOUNT_VALUE
    }

    def 'Anonymous customer should be able to apply voucher to order'()
    {
        when: 'customer applies voucher to  a cart'
        cartApi.applyVoucher(uid, cartGuid, ANONYMOUS_VOUCHER_CODE, accessToken)
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: cartTotal])

        and: 'customer places an order'
        def orderResponse = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        String orderCode = jsonSlurper.parseText(orderResponse.body).code
        Map salesMessage = waitAndReturnSalesMessageByCartGuid(cartGuid)

        then: 'voucher is applied to order and totals are adjusted accordingly'
        salesMessage.OrderId == orderCode
        salesMessage.Vouchers[0].VoucherCode == ANONYMOUS_VOUCHER_NAME
        salesMessage.Vouchers[0].Value == ANONYMOUS_VOUCHER_VALUE * -1
    }
}
