package apitests

import java.text.SimpleDateFormat

import data.annotations.Smoke
import spock.lang.Specification
import traits.WithCustomerApi

import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.FB_ACCOUNT
import static data.constants.Credentials.PASSWORD
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Payloads.USER_REVIEW_PAYLOAD
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static org.apache.http.HttpStatus.SC_BAD_REQUEST
import static org.apache.http.HttpStatus.SC_CREATED
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAccountApi
import static services.ApiClient.getAdminApi
import static services.ApiClient.getRdpApi
import static services.ApiClient.getStoreApi

class AccountTestSuite extends Specification implements WithCustomerApi
{
    static final String RANGE_REVIEW = 'AKIRA'
    static final String SOFOLOGIST_STORE_NAME = 'BATHGATE'
    static final String DEFAULT_STORE_NAME = 'INTERNET'
    static String uid
    static String addressPayload

    def setupSpec()
    {
        reportHeader '<h2>Account tests suite</h2>'
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        addCustomer(SOFOLOGIST_USER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
    }

    def setup()
    {
        uid = register()
    }

    def 'Customer should be able to login with facebook account'()
    {
        when: 'attempt to log in with facebook account'
        String accessToken = login(FB_ACCOUNT)
        def loginResponse = accountApi.loginByEmailFB(uid, accessToken)

        then: 'login is successful'
        loginResponse.httpStatus == SC_OK
    }

    @Smoke
    def 'Customer should be able to log in with registered account'()
    {
        when: 'registered account attempts to log in'
        String accessToken = login(uid)

        then: 'login is successful'
        accessToken != null
    }

    def 'Customer should be able to log out with registered account'()
    {
        given: 'registered account logged in'
        String accessToken = login(uid)

        when: 'attempts to log out'
        def logoutResponse = accountApi.logout(uid, accessToken)

        then: 'logout is successful'
        logoutResponse.httpStatus == SC_OK
    }

    @Smoke
    def 'Customer should be able to obtain my account details'()
    {
        given: 'registered account logged in'
        String accessToken = login(uid)

        when: 'requests my account details'
        def myProfileResponse = accountApi.myProfile(uid, accessToken)

        then: 'my account details are returned'
        myProfileResponse.httpStatus == SC_OK
        jsonSlurper.parseText(myProfileResponse.body).name == uid
    }

    @Smoke
    def 'Customer should not be able to log in with invalid credentials'()
    {
        when: 'customer attempts to log in with invalid user/password combinations'
        def loginResponse = accountApi.login(account, password)

        then: 'log in is refused'
        loginResponse.httpStatus == SC_BAD_REQUEST

        where: 'different invalid user/password combinations'
        account      | password
        uid          | 'invalidpassword'
        'invaliduid' | PASSWORD
        'invaliduid' | 'invalidpassword'
    }

    def 'Customer should be able to receive recently viewed range'()
    {
        given: 'registered account logged in'
        String accessToken = login(uid)

        when: 'performs rdp range search'
        rdpApi.searchAuthorized(RANGE_REVIEW, accessToken)
        def myProfileResponse = accountApi.myProfile(uid, accessToken)
        def recentlyViewedResponse = accountApi.recentlyViewed(accessToken)

        then: 'searched range is displayed in my account in recently viewed section'
        responseContains(myProfileResponse.body, RANGE_REVIEW)
        responseContains(recentlyViewedResponse.body, RANGE_REVIEW)
    }

    @Smoke
    def 'Customer should be able to add address to account'()
    {
        given: 'registered account logged in'
        String accessToken = login(uid)

        when: 'adds customer address'
        def response = accountApi.addAddress(uid, accessToken, addressPayload)

        then: 'address is added to account'
        response.httpStatus == SC_CREATED
        responseContains(response.body, '"lastName" : "TKLastName"')
    }

    def 'Customer should be able to retrieve stored account address'()
    {
        given: 'registered account with address added logged in'
        String accessToken = login(uid)
        accountApi.addAddress(uid, accessToken, addressPayload)

        when: 'requests customer address'
        def getResponse = accountApi.getAddress(uid, accessToken)

        then: 'customer address is returned'
        getResponse.httpStatus == SC_OK
        responseContains(getResponse.body, '"lastName" : "TKLastName"')
    }

    def 'Customer should be able to delete address from account'()
    {
        given: 'registered account with address added logged in'
        String accessToken = login(uid)
        accountApi.addAddress(uid, accessToken, addressPayload)

        when: 'deletes address from address book'
        def getResponse = accountApi.getAddress(uid, accessToken)
        String addressId = jsonSlurper.parseText(getResponse.body).addresses[0].id
        def deleteResponse = accountApi.deleteAddress(uid, addressId, accessToken)
        getResponse = accountApi.getAddress(uid, accessToken)

        then: 'address is removed and no longer available for account'
        deleteResponse.httpStatus == SC_OK
        !responseContains(getResponse.body, '"lastName" : "TKLastName"')
    }

    def 'Customer should be able to submit customer review'()
    {
        given: 'registered account logged in'
        String accessToken = login(uid)
        String timestamp = timestamp()
        String payload = preparePayload(USER_REVIEW_PAYLOAD, [
                timestamp: timestamp,
                uid      : uid
        ])

        when: 'submits customer review'
        def reviewResponse = accountApi.submitReview(accessToken, payload)
        def reviewPk = adminApi.flexSearch('findCustomerReviewByHeadline.fxs', [headline: timestamp])

        then: 'customer review is stored in the database'
        reviewResponse.httpStatus == SC_CREATED
        reviewPk != null
    }

    def 'Customer should be able to get submitted customer review'()
    {
        given: 'registered account with customer review submitted logged in'
        String accessToken = login(uid)
        String timestamp = timestamp()
        String payload = preparePayload(USER_REVIEW_PAYLOAD, [
                timestamp: timestamp,
                uid      : uid
        ])
        accountApi.submitReview(accessToken, payload)
        def reviewPk = adminApi.flexSearch('findCustomerReviewByHeadline.fxs', [headline: timestamp])
        adminApi.impexImport('approveCustomerReview.impex', [pk: reviewPk])

        when: 'requests customer review'
        def reviewResponse = accountApi.getReview(accessToken)
        def review = jsonSlurper.parseText(reviewResponse.body).reviews.find { it.headline.contains(timestamp) }

        then: 'customer review received with all details'
        reviewResponse.httpStatus == SC_OK
        review.category == 'Leather'
        review.comment == 'Here is the customer\'s comments for the review. Can be a long text'
        review.rating == 1.0
    }

    def 'Employee with customer manager role should be able to update customer email'()
    {
        given: 'login with employee with customer manage role'
        String accessToken = login(CUSTOMER_MANAGER)
        String newUid = "new_${uid}"

        when: 'updates customer email address/uid'
        def response = accountApi.updateEmail(uid, newUid, accessToken)
        def oldCustomerPk = adminApi.flexSearch('getCustomerByUid.fxs', [uid: uid])
        def newCustomerPk = adminApi.flexSearch('getCustomerByUid.fxs', [uid: newUid])

        then: 'customer email/uid is updated'
        response.httpStatus == SC_OK
        oldCustomerPk.isEmpty()
        !newCustomerPk.isEmpty()
    }

    def 'Existing customer should have current store associated with account'()
    {
        given: 'registered existing account logged in'
        String accessToken = login(SOFOLOGIST_USER)

        when: 'request to get current store'
        def response = storeApi.getCurrent(accessToken)

        then: 'customer selected store is received'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).name == SOFOLOGIST_STORE_NAME
    }

    @Smoke
    def 'New customer should have current store set as default'()
    {
        given: 'registered new account logged in'
        String accessToken = login(uid)

        when: 'request to get current store'
        def response = storeApi.getCurrent(accessToken)

        then: 'default store is received'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).name == DEFAULT_STORE_NAME
    }

    boolean responseContains(String response, String rangeReview)
    {
        response.contains(rangeReview)
    }

    String timestamp()
    {
        Date timestamp = Calendar.getInstance().getTime()
        SimpleDateFormat formatter = new SimpleDateFormat('yyyyMMddhhmmss')

        formatter.format(timestamp)
    }
}
