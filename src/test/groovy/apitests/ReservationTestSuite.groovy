package apitests

import groovy.json.JsonSlurper
import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Products.OUTLET_PRODUCT_EX_DISPLAY
import static data.constants.Products.OUTLET_PRODUCT_OVERSTOCK
import static data.constants.Products.OUTLET_PRODUCT_QUICKSTOCK
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_A
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_B
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.SuperCategories.EX_DISPLAY_CATEGORY
import static data.constants.SuperCategories.OVERSTOCK_CATEGORY
import static data.constants.SuperCategories.QUICKSTOCK_CATEGORY
import static data.constants.SuperCategories.REFURBISHED_CATEGORY
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getCartApi

class ReservationTestSuite extends Specification implements WithProductApi, WithCustomerApi
{
    static def jsonSlurper = new JsonSlurper()
    static final String PICK_UP_PRODUCT_ID = '615-37205'
    static final String PICK_UP_LOCATION = 'BATHGATE'
    static final String POSTAL_CODE = 'L13 5TB'
    static String uid
    static String accessToken
    static String cartGuid
    static String addressPayload
    static String addressId

    def setupSpec()
    {
        reportHeader '<h2>Reservation tests suite</h2>'
        addProduct(SIMPLE_PRODUCT)
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        uid = register()
        accessToken = login(uid)
        addressId = addAddress(uid, accessToken, addressPayload)
    }

    def setup()
    {
        cartGuid = cartApi.create(uid, accessToken)
    }

    def 'Customer should be able to add product to cart and reserve it'()
    {
        given: 'customer added product to cart'
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
        cartApi.setDeliveryPostalCode(uid, cartGuid, POSTAL_CODE, accessToken)
        cartApi.setDeliveryOption(uid, cartGuid, DELIVERY_OPTION_VAN, accessToken)

        when: 'product reservation confirmation is sent'
        def response = cartApi.confirmReservation(uid, cartGuid, accessToken)

        then: 'product reservation is confirmed'
        response.httpStatus == SC_OK

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to add pick up location in cart'()
    {
        given: 'customer added product to cart and confirmed reservation'
        cartApi.addEntry(uid, cartGuid, PICK_UP_PRODUCT_ID, accessToken)
        cartApi.setDeliveryPostalCode(uid, cartGuid, POSTAL_CODE, accessToken)
        cartApi.setDeliveryOption(uid, cartGuid, DELIVERY_OPTION_VAN, accessToken)
        cartApi.confirmReservation(uid, cartGuid, accessToken)

        when: 'add pick up location'
        def responsePickUp = cartApi.addPickUpLocation(uid, cartGuid, accessToken, PICK_UP_LOCATION)

        then: 'cart delivery is updated with a desired pick up location (pos)'
        responsePickUp.httpStatus == SC_OK
        jsonSlurper.parseText(responsePickUp.body).entry.deliveryPointOfService.name == PICK_UP_LOCATION
        jsonSlurper.parseText(responsePickUp.body).entry.product.availableForPickup == true

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to add outlet product to cart and reserve it - #stockType'()
    {
        given: 'customer added outlet product to cart'
        addOutletProduct(product, categoryId)
        cartApi.addOutletEntry(uid, cartGuid, product.variant, product.stockType, accessToken)
        cartApi.addDeliveryAddress(uid, cartGuid, addressId, accessToken)
        cartApi.setDeliveryOption(uid, cartGuid, DELIVERY_OPTION_VAN, accessToken)

        when: 'product reservation confirmation is sent'
        def response = cartApi.confirmReservation(uid, cartGuid, accessToken)

        then: 'product reservation is confirmed'
        response.httpStatus == SC_OK

        cleanup:
        cartApi.removeCart(uid, cartGuid, accessToken)

        where: 'different outlet product types'
        product                      | categoryId
        OUTLET_PRODUCT_OVERSTOCK     | OVERSTOCK_CATEGORY
        OUTLET_PRODUCT_QUICKSTOCK    | QUICKSTOCK_CATEGORY
        OUTLET_PRODUCT_EX_DISPLAY    | EX_DISPLAY_CATEGORY
        OUTLET_PRODUCT_REFURBISHED_A | REFURBISHED_CATEGORY
        OUTLET_PRODUCT_REFURBISHED_B | REFURBISHED_CATEGORY
    }

    String accessToken(String response)
    {
        jsonSlurper.parseText(response).access_token
    }
}
