package apitests

import groovy.json.JsonSlurper
import spock.lang.Specification

import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getLogisticsApi

class LogisticsTestSuite extends Specification
{
    def jsonSlurper = new JsonSlurper()

    def 'Logistics service should return main land delivery address and mode'()
    {
        given: 'main land postal code'
        def mainLandPostalCode = 'EH20 9PW'

        when: 'request logistic service to get delivery information by postal code'
        def response = logisticsApi.getDelivery(mainLandPostalCode)
        def logisticsResponse = parsedLogisticsResponse(response.body)

        then: 'delivery information is returned'
        response.httpStatus == SC_OK
        logisticsResponse.Address.Warehouse == 'BATHGATE'
        logisticsResponse.Address.NearestGeoWarehouse == 'BATHGATE'
        logisticsResponse.Address.Postcode == mainLandPostalCode
        logisticsResponse.DeliveryMode == 'Default'
    }

    def 'Logistics service should return non main land delivery address and mode'()
    {
        given: 'non main land postal code'
        def nonMainLandPostalCode = 'IV41 8AV'

        when: 'request logistic service to get delivery information by postal code'
        def response = logisticsApi.getDelivery(nonMainLandPostalCode)
        def logisticsResponse = parsedLogisticsResponse(response.body)

        then: 'delivery information is returned'
        response.httpStatus == SC_OK
        logisticsResponse.Address.Warehouse == 'AREA BLACKLIST'
        logisticsResponse.Address.Postcode == nonMainLandPostalCode
        logisticsResponse.DeliveryMode == 'NonMainland'
    }

    Map parsedLogisticsResponse(String response)
    {
        jsonSlurper.parseText(response) as Map
    }
}
