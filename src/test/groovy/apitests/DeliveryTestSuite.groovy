package apitests

import spock.lang.Specification
import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.DeliveryOptions.DELIVERY_OPTION_NON_MAINLAND_COURIER
import static data.constants.DeliveryOptions.DELIVERY_OPTION_NON_MAINLAND_STORE_PICK_UP
import static data.constants.DeliveryOptions.DELIVERY_OPTION_STORE_PICK_UP
import static data.constants.DeliveryOptions.DELIVERY_OPTION_VAN
import static data.constants.DeliveryOptions.DELIVERY_OPTION_WEEKEND
import static data.constants.Products.SIMPLE_PRODUCT
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAdminApi
import static services.ApiClient.getCartApi

class DeliveryTestSuite extends Specification implements WithCustomerApi, WithProductApi
{
    static final String DEFAULT_POSTAL_CODE = 'L13 5TB'
    static final String NON_MAINLAND_POSTAL_CODE = 'HS3 3DP'
    static String uid
    static String accessToken
    static String cartGuid

    def setupSpec()
    {
        reportHeader '<h2>Delivery tests suite</h2>'
        addProduct(SIMPLE_PRODUCT)
        uid = register()
        accessToken = login(uid)
    }

    def setup()
    {
        cartGuid = cartApi.create(uid, accessToken)
        cartApi.addEntry(uid, cartGuid, SIMPLE_PRODUCT.variant, accessToken)
    }

    def cleanup()
    {
        cartApi.removeCart(uid, cartGuid, accessToken)
    }

    def 'Customer should be able to apply #deliveryOption for main land delivery mode'()
    {
        given: 'customer creates cart with delivery postal set to main land'
        String deliveryModeCode = 'default'
        cartApi.setDeliveryPostalCode(uid, cartGuid, DEFAULT_POSTAL_CODE, accessToken)

        when: 'attempt to apply delivery option'
        cartApi.setDeliveryOption(uid, cartGuid, deliveryOption, accessToken)
        def response = cartApi.deliveryMode(uid, cartGuid, accessToken)
        def expectedDeliveryCost = deliveryModeCost(deliveryModeCode) + deliveryOptionCost(deliveryOption)

        then: 'delivery option is applied to cart and delivery cost calculated'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).code == deliveryModeCode
        jsonSlurper.parseText(response.body).deliveryCost.value == expectedDeliveryCost

        where: 'all eligible delivery option'
        deliveryOption                | _
        DELIVERY_OPTION_VAN           | _
        DELIVERY_OPTION_WEEKEND       | _
        DELIVERY_OPTION_STORE_PICK_UP | _
    }

    def 'Customer should be able to apply non main land delivery mode'()
    {
        given: 'non main land postal code'
        String deliveryModeCode = 'nonMainland'

        when: 'customer attempts to apply non main land postal code to a cart'
        cartApi.setDeliveryPostalCode(uid, cartGuid, NON_MAINLAND_POSTAL_CODE, accessToken)
        def response = cartApi.deliveryMode(uid, cartGuid, accessToken)

        then: 'non main land delivery mode is set and delivery cost calculated'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).code == deliveryModeCode
        jsonSlurper.parseText(response.body).deliveryCost.value == deliveryModeCost(deliveryModeCode)
    }

    def 'Customer should be able to apply #deliveryOption for non main land delivery mode'()
    {
        given: 'customer creates cart with delivery postal set to non main land'
        String deliveryModeCode = 'nonMainland'
        cartApi.setDeliveryPostalCode(uid, cartGuid, NON_MAINLAND_POSTAL_CODE, accessToken)

        when: 'attempt to apply delivery option'
        cartApi.setDeliveryOption(uid, cartGuid, deliveryOption, accessToken)
        def response = cartApi.deliveryMode(uid, cartGuid, accessToken)
        def expectedDeliveryCost = deliveryModeCost(deliveryModeCode) + deliveryOptionCost(deliveryOption)

        then: 'delivery option is applied to cart and delivery cost calculated'
        response.httpStatus == SC_OK
        jsonSlurper.parseText(response.body).code == deliveryModeCode
        jsonSlurper.parseText(response.body).deliveryCost.value == expectedDeliveryCost

        where: 'all eligible delivery option'
        deliveryOption                             | _
        DELIVERY_OPTION_NON_MAINLAND_COURIER       | _
        DELIVERY_OPTION_NON_MAINLAND_STORE_PICK_UP | _
    }

    BigDecimal deliveryModeCost(String deliveryMode)
    {
        String cost = adminApi.flexSearch('deliveryModeCost.fxs', [
                deliveryMode: deliveryMode
        ])
        new BigDecimal(cost)
    }

    BigDecimal deliveryOptionCost(String deliveryOption)
    {
        String cost = adminApi.flexSearch('deliveryOptionCost.fxs', [
                deliveryOption: deliveryOption
        ])
        new BigDecimal(cost)
    }
}
