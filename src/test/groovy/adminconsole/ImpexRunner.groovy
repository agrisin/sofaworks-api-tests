package adminconsole

import httphandler.AppResponse
import httphandler.AppResponseHandler
import org.apache.http.client.fluent.Request
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import traits.WithAdminApi

class ImpexRunner implements WithAdminApi
{
    static final String IMPEX_IMPORT_BASE_PATH = '/admin/console/impex/import'
    private static final RESOURCE_FOLDER = '/impex'

    private AppResponse response

    void fromTemplate(String fileName, Map map)
    {
        GString impexFile = "$RESOURCE_FOLDER/${fileName}"
        String impex = TemplatePopulator.populateTemplate(impexFile, map)
        response = execute(impex)

        assert impexResult().contains('Import finished successfully'): "Impex:\n${impex}\n\nMessage:\n${impexResult()}\n\nResult:\n${impexError()}"
    }

    AppResponse execute(String impex)
    {
        adminLogin()

        Map params = [
                scriptContent       : impex,
                validationEnum      : 'IMPORT_STRICT',
                maxThreads          : '1',
                encoding            : 'UTF-8',
                _legacyMode         : 'on',
                _enableCodeExecution: 'on'
        ]

        String url = buildUri(ADMIN, IMPEX_IMPORT_BASE_PATH, params)

        Request request = Request
                .Post(url)
                .addHeader('Cookie', jSession)
                .addHeader('X-CSRF-TOKEN', csrfToken)

        executor
                .use(cookieStore)
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    String impexResult()
    {
        Document doc = Jsoup.parse(response.body)
        doc.body().getElementById('impexResult').attr('data-result')
    }

    String impexError()
    {
        Document doc = Jsoup.parse(response.body)
        doc.body().getElementsByClass('impexResult').text()
    }
}
