package adminconsole

import groovy.json.JsonSlurper
import httphandler.AppResponse
import httphandler.AppResponseHandler
import org.apache.http.client.fluent.Request
import traits.WithAdminApi

class FlexibleSearchRunner implements WithAdminApi
{
    static final String FLEX_SEARCH_BASE_PATH = '/admin/console/flexsearch/execute'
    private static final RESOURCE_FOLDER = '/flexiblesearch'

    List fromTemplate(String fileName, Map map)
    {
        GString flexSearchFile = "$RESOURCE_FOLDER/${fileName}"

        String query = TemplatePopulator.populateTemplate(flexSearchFile, map)
        def jsonSlurper = new JsonSlurper()
        def response = execute(query)

        checkResponse(response)

        def results = jsonSlurper.parseText(response.body).resultList
        List resultsList = []
        results.each {
            resultsList += it
        }
        resultsList
    }

    AppResponse execute(String fsQuery, int maxCount = 10)
    {
        adminLogin()

        Map params = [
                flexibleSearchQuery: fsQuery,
                maxCount           : maxCount.toString()
        ]

        String url = buildUri(ADMIN, FLEX_SEARCH_BASE_PATH, params)

        Request request = Request
                .Post(url)
                .addHeader('Cookie', jSession)
                .addHeader('X-CSRF-TOKEN', csrfToken)

        executor
                .use(cookieStore)
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }
}
