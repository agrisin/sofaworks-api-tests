package adminconsole

import httphandler.AppResponse
import httphandler.AppResponseHandler
import org.apache.http.client.fluent.Request
import traits.WithAdminApi

class ScriptRunner implements WithAdminApi
{
    static final String SCRIPT_BASE_PATH = '/admin/console/scripting/execute'
    private static final RESOURCE_FOLDER = '/script'

    AppResponse fromTemplate(String fileName, Map map)
    {
        GString scriptFile = "$RESOURCE_FOLDER/${fileName}"

        String script = TemplatePopulator.populateTemplate(scriptFile, map)
        execute(script)
    }

    private AppResponse execute(String script, String scriptType = 'groovy')
    {
        adminLogin()

        Map params = [
                scriptType: scriptType,
                script    : script,
                commit    : 'true'
        ]

        String url = buildUri(ADMIN, SCRIPT_BASE_PATH, params)

        Request request = Request
                .Post(url)
                .addHeader('Cookie', jSession)
                .addHeader('X-CSRF-TOKEN', csrfToken)

        executor
                .use(cookieStore)
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }
}
