package adminconsole

import java.text.SimpleDateFormat

import ssh.SSHCommandExecute

import static services.ApiClient.getAdminApi

class StockImport
{
    private String productId
    private String warehouse
    private String stockType
    private String timestamp
    private String sodTimestamp
    final int SLEEP_TIME = 5000

    StockImport(String productId, String stockType, String warehouse = 'default')
    {
        this.productId = productId
        this.warehouse = warehouse
        this.stockType = stockType
    }

    void execute(int quantity = 10)
    {
        sodTimestamp = timestamp(true)
        adminApi.impexImport('resetStockImportSequence.impex', [timestamp: sodTimestamp])
        SSHCommandExecute sshCommandExecute = new SSHCommandExecute()
        timestamp = timestamp()
        String command = createStockImportFile(quantity)
        sshCommandExecute.execute(command)
        sleep(SLEEP_TIME)
        command = moveStockImportFile()
        sshCommandExecute.execute(command)
        sleep(SLEEP_TIME)
    }

    private String createStockImportFile(int quantity)
    {
        "sudo su - hybris -c \"echo '${productId};${quantity};14;${warehouse};${stockType}' > /opt/hybris/current/hybris/data/dataimport/store/master/stock/archive/stock-${timestamp}.csv\""
    }

    private String moveStockImportFile()
    {
        "sudo su - hybris -c \"mv /opt/hybris/current/hybris/data/dataimport/store/master/stock/archive/stock-${timestamp}.csv /opt/hybris/current/hybris/data/dataimport/store/master/stock/\""
    }

    private String timestamp(boolean isStartOfDay = false)
    {
        TimeZone timeZone = TimeZone.getTimeZone('UTC')
        Calendar timestampCalendar = Calendar.getInstance()
        if (isStartOfDay)
        {
            timestampCalendar.set(Calendar.HOUR_OF_DAY, 4)
            timestampCalendar.set(Calendar.MINUTE, 0)
            timestampCalendar.set(Calendar.SECOND, 0)
        }
        SimpleDateFormat formatter = new SimpleDateFormat('yyyyMMddhhmmss')
        formatter.setTimeZone(timeZone)

        formatter.format(timestampCalendar.getTime())
    }
}
