package httphandler

import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext

import org.apache.http.client.HttpClient
import org.apache.http.client.fluent.Executor
import org.apache.http.config.Registry
import org.apache.http.config.RegistryBuilder
import org.apache.http.conn.socket.ConnectionSocketFactory
import org.apache.http.conn.socket.PlainConnectionSocketFactory
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.conn.ssl.TrustStrategy
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.LaxRedirectStrategy
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.ssl.SSLContextBuilder

class HttpExecutor
{
    static Executor instance()
    {
        HttpClient httpClient = prepareHttpClient()
        Executor.newInstance(httpClient)
    }

    private static HttpClient prepareHttpClient()
    {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create()
        SSLContext sslContext = buildSSLContext()
        httpClientBuilder
                .setSSLContext(sslContext)
                .setRedirectStrategy(new LaxRedirectStrategy())

        Registry<ConnectionSocketFactory> socketFactoryRegistry = buildSocketFactoryRegistry(sslContext)

        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry)
        httpClientBuilder.setConnectionManager(connMgr)

        httpClientBuilder.build()
    }

    private static SSLContext buildSSLContext()
    {
        new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
            boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
            {
                return true;
            }
        }).build()
    }

    private static Registry buildSocketFactoryRegistry(SSLContext sslContext)
    {
        HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier)
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                .<ConnectionSocketFactory> create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory).build()
        socketFactoryRegistry
    }
}
