package httphandler

import static org.apache.http.HttpStatus.SC_OK

class AppResponse
{
    int httpStatus = SC_OK
    String body = ''

    AppResponse(int httpStatus, String body)
    {
        this.httpStatus = httpStatus
        this.body = body
    }
}
