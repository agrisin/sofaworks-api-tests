package traits

import groovy.json.JsonSlurper

import static data.constants.Credentials.PASSWORD
import static org.apache.http.HttpStatus.SC_CREATED
import static services.ApiClient.getAccountApi
import static services.ApiClient.getAdminApi

trait WithCustomerApi implements WithAdminApi
{
    static def jsonSlurper = new JsonSlurper()

    void addCustomer(String uid, String password = PASSWORD)
    {
        adminApi.impexImport('addCustomer.impex', [
                uid     : uid,
                password: password
        ])
    }

    void addEmployee(String employeeId, String group, String password = PASSWORD)
    {
        adminApi.impexImport('addEmployee.impex', [
                employeeId: employeeId,
                password  : password,
                group     : group
        ])
    }

    void addCustomerAddress(String uid, String postalCode = 'SW7', String phoneNumber = '1234567890')
    {
        adminApi.impexImport('customerAddressAdd.impex', [
                uid        : uid,
                postalCode : postalCode,
                phoneNumber: phoneNumber
        ])
    }

    String register()
    {
        Date dateTime = new Date()
        String uid = "sofology_swk_${dateTime.getTime()}@tacitknowledge.com"
        def registerResponse = accountApi.register(uid, PASSWORD)
        assert registerResponse.httpStatus == SC_CREATED
        uid
    }

    String login(String uid, String password = PASSWORD)
    {
        def loginResponse = accountApi.login(uid, password)
        checkResponse(loginResponse)
        accessToken(loginResponse.body)
    }

    String addAddress(String uid, String accessToken, String addressPayload)
    {
        def addAddressResponse = accountApi.addAddress(uid, accessToken, addressPayload)
        jsonSlurper.parseText(addAddressResponse.body).id as String
    }

    String accessToken(String response)
    {
        jsonSlurper.parseText(response).access_token
    }
}