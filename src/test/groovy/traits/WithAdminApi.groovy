package traits

import httphandler.HttpExecutor
import org.apache.http.client.CookieStore
import org.apache.http.client.fluent.Executor
import org.apache.http.client.fluent.Request
import org.apache.http.cookie.Cookie
import org.apache.http.impl.client.BasicCookieStore

trait WithAdminApi implements WithApiHelper
{
    private static final String LOGIN = 'admin'
    private static final String PASSWORD = 'nimda'
    private static final String CSRF_REGEX = '<meta name=\"_csrf\" content=\"([^\"]+)'

    static final String ADMIN_LOGIN_PAGE_BASE_PATH = '/admin/login.jsp'
    static final String ADMIN_LOGIN_BASE_PATH = '/admin/j_spring_security_check'

    CookieStore cookieStore = new BasicCookieStore()
    Executor executor = HttpExecutor.instance()

    String csrfToken
    String jSession

    void adminLogin()
    {
        openLoginPage()
        loginRequest()
    }

    void openLoginPage()
    {
        String url = buildUri(ADMIN, ADMIN_LOGIN_PAGE_BASE_PATH)
        String response = executeRequestWithCookies(Request.Get(url), cookieStore).body
        updateCsrf(response)
    }

    void loginRequest()
    {
        Map params = [
                'j_username': LOGIN,
                'j_password': PASSWORD,
                'submit'    : 'login',
                '_csrf'     : csrfToken
        ]

        String url = buildUri(ADMIN, ADMIN_LOGIN_BASE_PATH, params)

        String response = executeRequestWithCookies(Request.Post(url), cookieStore).body

        assert !response.contains('Login failed'): 'Hybris Admin Console login failed'

        updateCsrf(response)
        updateJSession()
    }

    private void updateCsrf(String response)
    {
        csrfToken = (response =~ /$CSRF_REGEX/)[0][1]
    }

    private void updateJSession()
    {
        Cookie jSessionCookie = cookieStore.getCookies().find {
            it.name == 'JSESSIONID'
        }
        jSession = "${jSessionCookie.name}=${jSessionCookie.value}"
    }
}