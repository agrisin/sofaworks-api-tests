package traits

import data.dto.Category
import data.dto.Product

import static services.ApiClient.getAdminApi

trait WithProductApi implements WithAdminApi
{
    static final String SUPPLIER_NAME = 'AUTO_TEST_SUPPLIER'
    static final String SUPPLIER_RANGE = 'AUTO_TEST_SUPPLIER_RANGE'

    void addProduct(Product product, String version = 'Online')
    {
        adminApi.impexImport('addProductFull.impex', [
                productCode   : product.base,
                variantCode   : product.variant,
                swkVariantCode: "${product.variant}_SWK",
                productPrice  : product.price,
                stockType     : product.stockType,
                version       : version,
                supplierName  : SUPPLIER_NAME,
                supplierRange : SUPPLIER_RANGE
        ])
    }

    void addProductReferences(String bundleProduct, String product1, String product2, int qty = 1)
    {
        adminApi.impexImport('addProductReference.impex', [
                code    : bundleProduct,
                variant1: product1,
                variant2: product2,
                qty     : qty
        ])
    }

    void removeProductReferences(String productCode)
    {
        adminApi.impexImport('removeProductReference.impex', [
                code: productCode
        ])
    }

    void removeProduct(Product product, String version = 'Online')
    {
        adminApi.impexImport('removeProduct.impex', [
                productCode   : product.base,
                variantCode   : product.variant,
                swkVariantCode: "${product.variant}_SWK",
                version       : version
        ])
    }

    void addOutletProduct(Product product, String category, String warehouse = 'BATHGATE', String version = 'Online')
    {
        adminApi.impexImport('addOutletProduct.impex', [
                productCode   : product.base,
                variantCode   : product.variant,
                swkVariantCode: "${product.variant}_SWK",
                productPrice  : product.price,
                stockType     : product.stockType,
                version       : version,
                warehouse     : warehouse,
                categoryName  : category,
                supplierName  : SUPPLIER_NAME,
                supplierRange : SUPPLIER_RANGE
        ])
    }

    void addOccasionalProduct(Product product, String version = 'Online')
    {
        adminApi.impexImport('addOccasionalProduct.impex', [
                productCode   : product.base,
                variantCode   : product.variant,
                swkVariantCode: "${product.variant}_Swk",
                productPrice  : product.price,
                stockType     : product.stockType,
                version       : version
        ])
    }

    void addSuperCategory(String categoryId, String version = 'Online')
    {
        adminApi.impexImport('addCategory.impex', [
                categoryId: categoryId,
                version   : version
        ])
    }

    void addCategory(Category category)
    {
        adminApi.impexImport('addCategoryFull.impex', [
                variantCategoryId   : category.variant,
                variantCategoryValue: category.variantValue,
                superCategory       : category.superCategory
        ])
    }

    void addProductCategoryRelation(String productId, String categoryId)
    {
        adminApi.impexImport('addCategory2ProductRelation.impex', [
                categoryName: categoryId,
                productName : productId
        ])
    }

    String productInCategory(String productId, String categoryId, String version = 'Online')
    {
        adminApi.flexSearch('productInCategory.fxs', [
                catalogVersion: version,
                catalogId     : categoryId,
                productId     : productId
        ])
    }

    void addUpdateStock(Product product, String warehouse = 'BATHGATE')
    {
        adminApi.impexImport('addUpdateStock.impex', [
                productCode: product.variant,
                stockType  : product.stockType,
                warehouse  : warehouse
        ])
    }

    void addUpdatePrice(Product product, BigDecimal price = 100.0)
    {
        adminApi.impexImport('addPriceRow.impex', [
                productCode: product.variant,
                stockType  : product.stockType,
                priceValue : price
        ])
    }

    void addVoucher(String code, String name, BigDecimal value)
    {
        adminApi.impexImport('addVoucher.impex', [
                voucherName: name,
                voucherCode: code,
                value      : value
        ])
    }

    void addMultivalueSerialVoucher(String voucherCode, BigDecimal voucherValue)
    {
        adminApi.impexImport('addMultivalueSerialVoucher.impex', [
                voucherCode : voucherCode,
                voucherValue: voucherValue
        ])
    }

    void addDiscount(String discountCode, BigDecimal value)
    {
        adminApi.impexImport('addDiscount.impex', [
                discountCode: discountCode,
                value       : value
        ])
    }

    void addFreeGiftPromotion(String promoCode)
    {
        adminApi.impexImport('addFreeGiftPromotion.impex', [
                promoCode: promoCode
        ])
    }

    List findSuitableProducts(int numberOfProducts)
    {
        List products = adminApi.flexSearchMultipleResult('suitableProducts.fxs')
        Collections.shuffle(products)
        products.subList(0, numberOfProducts)
    }
}