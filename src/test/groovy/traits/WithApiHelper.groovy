package traits

import adminconsole.TemplatePopulator
import httphandler.AppResponse
import httphandler.AppResponseHandler
import httphandler.HttpExecutor
import org.apache.http.NameValuePair
import org.apache.http.client.CookieStore
import org.apache.http.client.fluent.Executor
import org.apache.http.client.fluent.Request
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.ContentType
import org.apache.http.message.BasicNameValuePair
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import static data.constants.Credentials.ADMIN_LOGIN
import static data.constants.Credentials.ADMIN_PASSWORD
import static data.constants.Credentials.OMS_RABBITMQ_ACCOUNT
import static data.constants.Credentials.OMS_RABBITMQ_PASSWORD
import static data.environment.Environment.getInstance
import static org.apache.http.entity.ContentType.APPLICATION_JSON

trait WithApiHelper
{
    static Logger logger = LogManager.getLogger(WithApiHelper.class)

    final String STORE = instance.baseUrl
    final String SECURED = instance.securedBaseUrl
    final String ADMIN = instance.adminUrl
    final String SOLR = instance.solrUrl
    final String OMS = instance.omsUrl
    final String LOGISTICS = instance.logisticsUrl

    static final Integer CONNECTION_TIMEOUT = 60000
    static final Integer SOCKET_TIMEOUT = 60000

    Executor executor = HttpExecutor.instance()

    String buildUri(String host, String basePath, Map params = [:])
    {
        HttpGet someHttpGet = new HttpGet("$host$basePath")

        List<NameValuePair> nvpList = new ArrayList<>(params.size())
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            nvpList.add(new BasicNameValuePair(entry.getKey(), entry.getValue()))
        }

        new URIBuilder(someHttpGet.getURI())
                .addParameters(nvpList)
                .build().toString()
    }

    AppResponse getRequest(String host, String basePath, params = [:])
    {
        Request request = Request.Get(buildUri(host, basePath, params))
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)

        executeRequest(request)
    }

    AppResponse postRequest(String host, String basePath, params = [:])
    {
        Request request = Request.Post(buildUri(host, basePath, params))
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)

        executeRequest(request)
    }

    AppResponse getRequestWithAuth(String host, String basePath, String accessToken, params = [:])
    {
        String uri = buildUri(host, basePath, params)

        Request request = Request
                .Get(uri)
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)
                .addHeader('Authorization', 'Bearer ' + accessToken)

        executeRequest(request)
    }

    AppResponse postRequestWithAuth(String host, String basePath, String accessToken, Map params = [:], String payload = '', ContentType contentType = APPLICATION_JSON)
    {
        String uri = buildUri(host, basePath, params)

        Request request = Request
                .Post(uri)
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)
                .addHeader('Authorization', 'Bearer ' + accessToken)

        if (!payload.isEmpty())
        {
            request.bodyString(payload, contentType)
        }

        executeRequest(request)
    }

    AppResponse patchRequestWithAuth(String host, String basePath, String accessToken, Map params = [:], String payload = '', ContentType contentType = APPLICATION_JSON)
    {
        String uri = buildUri(host, basePath, params)

        Request request = Request
                .Patch(uri)
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)
                .addHeader('Authorization', 'Bearer ' + accessToken)

        if (!payload.isEmpty())
        {
            request.bodyString(payload, contentType)
        }

        executeRequest(request)
    }

    AppResponse putRequestWithAuth(String host, String basePath, String accessToken, Map params = [:], String payload = '', ContentType contentType = APPLICATION_JSON)
    {
        String uri = buildUri(host, basePath, params)

        Request request = Request
                .Put(uri)
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)
                .addHeader('Authorization', 'Bearer ' + accessToken)

        if (!payload.isEmpty())
        {
            request.bodyString(payload, contentType)
        }

        executeRequest(request)
    }

    AppResponse deleteRequestWithAuth(String host, String basePath, String accessToken, params = [:])
    {
        String uri = buildUri(host, basePath, params)

        Request request = Request
                .Delete(uri)
                .connectTimeout(CONNECTION_TIMEOUT)
                .socketTimeout(SOCKET_TIMEOUT)
                .addHeader('Authorization', 'Bearer ' + accessToken)

        executeRequest(request)
    }

    AppResponse executeRequest(Request request)
    {
        executor
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    AppResponse executeRequestWithCookies(Request request, CookieStore cookieStore)
    {
        executor
                .use(cookieStore)
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    AppResponse getRequestWithBasiAuth(String host, String basePath)
    {
        String uri = buildUri(host, basePath)

        def authString = "${ADMIN_LOGIN}:${ADMIN_PASSWORD}".getBytes().encodeBase64().toString()

        Request request = Request
                .Get(uri)
                .addHeader('Authorization', 'Basic ' + authString)

        executor
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    AppResponse postOmsRequest(String host, String basePath, String payload = '', ContentType contentType = APPLICATION_JSON)
    {
        String uri = buildUri(host, basePath)

        def authString = "${OMS_RABBITMQ_ACCOUNT}:${OMS_RABBITMQ_PASSWORD}".getBytes().encodeBase64().toString()

        Request request = Request
                .Post(uri)
                .addHeader('Authorization', 'Basic ' + authString)

        if (!payload.isEmpty())
        {
            request.bodyString(payload, contentType)
        }

        executor
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    AppResponse putRequestWithBasiAuth(String host, String basePath, String payload = '', ContentType contentType = APPLICATION_JSON)
    {
        String uri = buildUri(host, basePath)

        def authString = "${ADMIN_LOGIN}:${ADMIN_PASSWORD}".getBytes().encodeBase64().toString()

        Request request = Request
                .Put(uri)
                .addHeader('Authorization', 'Basic ' + authString)

        if (!payload.isEmpty())
        {
            request.bodyString(payload, contentType)
        }

        executor
                .execute(request)
                .handleResponse(new AppResponseHandler())
    }

    String preparePayload(String payloadName, Map query = [:])
    {
        GString payloadTemplate = "/payload/${payloadName}"
        TemplatePopulator.populateTemplate(payloadTemplate, query)
    }

    void checkResponse(AppResponse response)
    {
        assert isSuccessfulResponse(response.httpStatus) : "Response received: ${response.httpStatus}\nBody: ${response.body}"
    }

    boolean isSuccessfulResponse(int status)
    {
        Integer.parseInt(Integer.toString(status).substring(0, 1)) == 2
    }
}