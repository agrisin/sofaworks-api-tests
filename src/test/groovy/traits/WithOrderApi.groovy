package traits

import groovy.json.JsonSlurper
import spock.util.concurrent.PollingConditions

import static data.constants.Channels.CHANNEL_DESKTOP
import static data.constants.Payloads.ADDRESS_PAYLOAD
import static data.constants.Payloads.APPLY_FINANCE_PAYLOAD
import static data.constants.Payloads.ORDER_PAYMENT_PAYLOAD
import static org.apache.http.HttpStatus.SC_OK
import static services.ApiClient.getAccountApi
import static services.ApiClient.getCartApi
import static services.ApiClient.getFinanceApi
import static services.ApiClient.getOmsApi
import static services.ApiClient.getOrderApi

trait WithOrderApi implements WithApiHelper
{
    static def jsonSlurper = new JsonSlurper()
    static PollingConditions conditions = new PollingConditions(timeout: 60, initialDelay: 2, delay: 2)

    static String addressPayload

    String createCartWithProduct(String productVariant, String uid, String accessToken)
    {
        addressPayload = preparePayload(ADDRESS_PAYLOAD)
        String cartGuid = cartApi.create(uid, accessToken)
        cartApi.addEntry(uid, cartGuid, productVariant, accessToken)
        def addAddressResponse = accountApi.addAddress(uid, accessToken, addressPayload)
        def addressId = jsonSlurper.parseText(addAddressResponse.body).id as String
        cartApi.addDeliveryAddress(uid, cartGuid, addressId, accessToken)
        cartGuid
    }

    String createAndConfirmCart(String productVariant, String uid, String accessToken, String deliveryOption)
    {
        String cartGuid = createCartWithProduct(productVariant, uid, accessToken)
        cartApi.setDeliveryOption(uid, cartGuid, deliveryOption, accessToken)
        cartApi.confirmReservation(uid, cartGuid, accessToken)
        cartGuid
    }

    Map waitAndReturnFinanceMessageByCartGuid(String cartGuid)
    {
        conditions.eventually {
            assert financeMessage(cartGuid) != null: 'finance queue message not found'
        }
        String message = financeMessage(cartGuid)
        jsonSlurper.parseText(message) as Map
    }

    String financeMessage(String cartGuid)
    {
        def response = omsApi.getQueue()
        def message = jsonSlurper.parseText(response.body).payload.find {
            jsonSlurper.parseText(it).Message.Id == cartGuid
        }
        message == null ? '' : message
    }

    Map waitAndReturnSalesMessageByCartGuid(String cartGuid)
    {
        conditions.eventually {
            assert salesMessage(cartGuid) != null: 'sales queue message not found'
        }
        String message = salesMessage(cartGuid)
        jsonSlurper.parseText(message) as Map
    }

    private String salesMessage(String cartGuid)
    {
        def response = omsApi.getCreateOrderQueue()
        jsonSlurper.parseText(response.body).payload.find {
            jsonSlurper.parseText(it).CartId == cartGuid
        }
    }

    String applyFinance(String uid, String cartGuid, String accessToken, String financeType, BigDecimal minDeposit, String submitValue = '')
    {
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        String applyFinancePayload = financePayload(cartGuid, cartTotal, financeType, minDeposit)
        def response = financeApi.applyFinance(uid, cartGuid, submitValue, applyFinancePayload, accessToken)

        response.body.isEmpty() ? '' : jsonSlurper.parseText(response.body).code
    }

    String applyFinance(String uid, String cartGuid, String accessToken, String financeType, String submitValue = '')
    {
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        def minDeposit = cartTotal / 10
        String applyFinancePayload = financePayload(cartGuid, cartTotal, financeType, minDeposit)
        def response = financeApi.applyFinance(uid, cartGuid, submitValue, applyFinancePayload, accessToken)

        response.body.isEmpty() ? '' : jsonSlurper.parseText(response.body).code
    }

    String placeOrderWithDeposit(String uid, String cartGuid, String accessToken, String financeType, BigDecimal minDeposit)
    {
        applyFinance(uid, cartGuid, accessToken, financeType, minDeposit)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: minDeposit])
        def response = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        jsonSlurper.parseText(response.body).code
    }

    String placeOrderWithFinanceApplied(String uid, String cartGuid, String accessToken, String financeType)
    {
        String submitValue = ''
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        def minDeposit = (cartTotal / 10)
        String applyFinancePayload = financePayload(cartGuid, cartTotal, financeType, minDeposit)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: minDeposit])
        financeApi.applyFinance(uid, cartGuid, submitValue, applyFinancePayload, accessToken)
        def response = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        jsonSlurper.parseText(response.body).code
    }

    String placeOrder(String uid, String cartGuid, String accessToken)
    {
        def cartTotal = totalCartPrice(uid, cartGuid, accessToken)
        def minDeposit = (cartTotal / 10)
        String paymentPayload = preparePayload(ORDER_PAYMENT_PAYLOAD, [paymentAmount: minDeposit])
        def response = orderApi.place(uid, cartGuid, CHANNEL_DESKTOP, accessToken, paymentPayload)
        jsonSlurper.parseText(response.body).code
    }

    String financePayload(String cartGuid, BigDecimal total, String financeType, BigDecimal deposit = total / 10)
    {
        preparePayload(APPLY_FINANCE_PAYLOAD, [
                cartGuid   : cartGuid,
                cartTotal  : total,
                deposit    : deposit,
                financeType: financeType
        ])
    }

    BigDecimal totalCartPrice(String uid, String cartGuid, String accessToken)
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).totalPriceWithTax.value
    }

    int itemQuantity(String uid, String cartGuid, String accessToken)
    {
        def getCart = cartApi.review(uid, cartGuid, accessToken)
        jsonSlurper.parseText(getCart.body).deliveryOrderGroups.entries[0][0].quantity
    }

    void checkOrderStatus(String orderCode, String expectedStatus)
    {
        conditions.eventually {
            def orderStatus = orderStatus(orderCode)
            (orderStatus == expectedStatus)
        }
    }

    private String orderStatus(String orderCode)
    {
        def orderDetails = orderApi.details(orderCode)
        assert orderDetails.httpStatus == SC_OK
        def order = new XmlSlurper().parseText(orderDetails.body)
        order.status
    }
}