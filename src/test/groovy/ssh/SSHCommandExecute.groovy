package ssh

import ssh.PublicKeySshSession

import static data.constants.SSHConfig.SSH_KEY_PATH
import static data.constants.SSHConfig.SSH_USER
import static data.environment.Environment.getInstance

class SSHCommandExecute
{
    private PublicKeySshSession session

    String execute(String command)
    {
        sshSessionBuild()
        session.connect()
        session.execute(command)
        session.disconnect()
        session.output
    }

    private PublicKeySshSession sshSessionBuild()
    {
        session = new PublicKeySshSession.Builder(
                instance.ssh,
                SSH_USER,
                SSH_KEY_PATH)
                .build()
    }
}
