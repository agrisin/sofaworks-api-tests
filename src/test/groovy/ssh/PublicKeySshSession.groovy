package ssh

import java.nio.file.Path
import java.nio.file.Paths

import com.jcraft.jsch.Channel
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.JSch
import com.jcraft.jsch.JSchException
import com.jcraft.jsch.Session

class PublicKeySshSession
{

    final Session session
    String output

    PublicKeySshSession(final Builder builder)
    {
        this.session = builder.jschSession
    }

    void connect()
    {
        if (session == null)
        {
            throw new IllegalArgumentException("Session object is null.")
        }

        session.connect()
    }

    void disconnect()
    {
        session.disconnect()
    }

    void execute(String command)
    {
        try
        {
            Channel channel = session.openChannel("exec")
            ((ChannelExec) channel).setCommand(command)
            ((ChannelExec) channel).setPty(false)
            channel.connect()
            readChannelOutput(channel)
            channel.disconnect()
        }

        catch (JSchException e)
        {
            println e
            throw new RuntimeException("Error durring SSH command execution. Command: " + command)
        }
    }

    private void readChannelOutput(Channel channel)
    {

        byte[] buffer = new byte[1024];

        try
        {
            InputStream input = channel.getInputStream()
            String line = "";
            while (true)
            {
                while (input.available() > 0)
                {
                    int i = input.read(buffer, 0, 1024);
                    if (i < 0)
                    {
                        break;
                    }
                    line = new String(buffer, 0, i);
                    output = line
                }

                if (line.contains("logout"))
                {
                    break;
                }

                if (channel.isClosed())
                {
                    break;
                }
                try
                {
                    Thread.sleep(1000);
                }
                catch (Exception e)
                {
                    println("Error while reading channel output: " + e)
                }
            }
        }
        catch (Exception e)
        {
            println("Error while reading channel output: " + e);
        }
    }

    static class Builder
    {
        private String host
        private String username
        private Path privateKeyPath
        private Session jschSession

        Builder(String host, String username, String path)
        {
            this.host = host
            this.username = username
            this.privateKeyPath = Paths.get(path)
        }

        PublicKeySshSession build()
        {
            JSch jsch = new JSch()

            Session session = null

            try
            {

                jsch.addIdentity(privateKeyPath.toString())

                session = jsch.getSession(username, host)
                session.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password")

                Properties config = new Properties()
                config.put("StrictHostKeyChecking", "no")

                session.setConfig(config)
            }
            catch (JSchException e)
            {
                throw new RuntimeException("Failed to create Jsch Session object.", e)
            }

            this.jschSession = session

            return new PublicKeySshSession(this)
        }
    }
}
