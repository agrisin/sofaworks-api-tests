package data.constants

class UserGroups
{
    static final String PRICING_USER_GROUP = 'pricingusergroup'
    static final String MERCH_USER_GROUP = 'merchandisingGroup'
    static final String CUSTOMER_MANAGER_GROUP = 'customermanagergroup'
    static final String TRUSTED_AUTHENTICATOR_GROUP = 'trustedAuthenticator'
}
