package data.constants

class SSHConfig
{
    static final SSH_USER = 'deployer'
    static final SSH_KEY_PATH = 'src/test/groovy/ssh/deployer-ssh_key'
    static final SSH_KEY_PATH_OLD = '/var/lib/jenkins/.ssh/deployer-ssh_key'
}
