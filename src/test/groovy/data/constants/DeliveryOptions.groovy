package data.constants

class DeliveryOptions
{
    static final String DELIVERY_OPTION_VAN = 'van'
    static final String DELIVERY_OPTION_WEEKEND = 'weekend'
    static final String DELIVERY_OPTION_STORE_PICK_UP = 'storePickupForOccasionalsAndFootstools'
    static final String DELIVERY_OPTION_NON_MAINLAND_COURIER = 'nonMainlandCourier'
    static final String DELIVERY_OPTION_NON_MAINLAND_STORE_PICK_UP = 'nonMainlandStorePickUp'
}
