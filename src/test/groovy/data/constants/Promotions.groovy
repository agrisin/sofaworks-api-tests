package data.constants

class Promotions
{
    static final String PROMO_CODE = 'freeNeal'
    static final String PROMO_PRODUCT = 'nealToyVariant'

    static final String ANONYMOUS_VOUCHER_NAME = 'AUTO_TEST_ANONYMOUS_VOUCHER'
    static final String ANONYMOUS_VOUCHER_CODE = '001-TKMD-TKMD-TKMD'
    static final BigDecimal ANONYMOUS_VOUCHER_VALUE = 20.0

    static final String DISCOUNT_CODE = 'AUTO_TEST_ANONYMOUS_DISCOUNT'
    static final BigDecimal DISCOUNT_VALUE = 10.0

    static final String MULTI_VALUE_VOUCHER_CODE = 'E13'
    static final BigDecimal MULTI_VALUE_VOUCHER_VALUE = 100.0
}
