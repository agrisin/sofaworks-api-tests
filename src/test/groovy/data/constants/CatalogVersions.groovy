package data.constants

class CatalogVersions
{
    static final String CATALOG_VERSION_STAGED = 'Staged'
    static final String CATALOG_VERSION_ONLINE = 'Online'
}
