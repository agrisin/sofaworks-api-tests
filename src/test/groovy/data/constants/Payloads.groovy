package data.constants

class Payloads
{
    static final String ADDRESS_PAYLOAD = '/address.json'
    static final String MULTI_PAYMENT_PAYLOAD = '/multi_payment.json'
    static final String PUBLISH_FINANCE_PAYLOAD = '/publish_finance.json'
    static final String FINANCE_PROVIDER_MESSAGE_PAYLOAD = '/finance_provider_message.json'
    static final String APPLY_FINANCE_PAYLOAD = '/apply_finance.json'
    static final String UPDATE_FINANCE_PAYLOAD = '/update_finance.json'
    static final String GET_FINANCE_QUEUE_PAYLOAD = '/get_finance_queue.json'
    static final String ORDER_PAYMENT_PAYLOAD = '/order_payment.json'
    static final String GET_CREATE_ORDER_QUEUE_PAYLOAD = '/get_create_order_queue.json'
    static final String CART_DISCOUNT_PAYLOAD = '/cart_discount.json'
    static final String CURATION_LIST_PAYLOAD = '/curation_list.json'
    static final String USER_REVIEW_PAYLOAD = '/userReview.xml'
    static final String PRODUCT_PRICE_PAYLOAD = '/product_price_update.json'
    static final String PRODUCT_FIELDS_PAYLOAD = '/product_fields_update.json'
}
