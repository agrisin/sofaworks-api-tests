package data.constants

class Environments
{
    static final LOCAL = 'local'
    static final SMOKE = 'smoke'
    static final QA = 'qa'
    static final MASTER = 'master'
    static final UAT = 'uat'
}
