package data.constants

class OrderStatuses
{
    static final String COMPLETED = 'COMPLETED'
    static final String INSUFFICIENT_DEPOSIT = 'INSUFFICIENT_DEPOSIT'
    static final String PROCESSING_ERROR = 'PROCESSING_ERROR'
    static final String FINANCE_DECLINED = 'FINANCE_DECLINED'
}
