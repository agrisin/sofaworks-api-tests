package data.constants

class FinanceTypes
{
    static final String FINANCE_TYPE_BNPL = 'BNPL'
    static final String FINANCE_TYPE_IFC = 'IFC'
}
