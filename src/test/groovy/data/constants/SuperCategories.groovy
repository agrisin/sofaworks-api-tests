package data.constants

class SuperCategories
{
    static final String FOOTSTOOL_CATEGORY = 'FootStool'
    static final String UPHOLSTERY_CATEGORY = 'Upholstery'
    static final String OCCASIONAL_CATEGORY = 'occasionalProducts'
    static final String OVERSTOCK_CATEGORY = 'Overstock'
    static final String QUICKSTOCK_CATEGORY = 'Quickstock'
    static final String EX_DISPLAY_CATEGORY = 'ExDisplay'
    static final String REFURBISHED_CATEGORY = 'Refurbished'
}