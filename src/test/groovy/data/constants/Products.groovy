package data.constants

import data.dto.Product

class Products
{
    final static Product SIMPLE_PRODUCT = new Product('AUTO_TEST_SIMPLE', 'AUTO_TEST_SIMPLE_VARIANT', 1000.0)
    final static Product BUNDLE_OUTLET_PRODUCT = new Product('AUTO_TEST_BUNDLE', 'AUTO_TEST_SIMPLE_BUNDLE', 500.0, 'EX_DISPLAY')
    final static Product SECOND_PRODUCT = new Product('AUTO_TEST_SIMPLE_2', 'AUTO_TEST_SIMPLE_VARIANT_2', 500.0)
    final static Product UPHOLSTERY_FOOTSTOOL_PRODUCT = new Product('AUTO_UPHOLSTERY_FOOTSTOOL', 'AUTO_UPHOLSTERY_FOOTSTOOL_VARIANT', 500.0)
    final static Product SOFASHIELD_PRODUCT = new Product('sofaShieldVariant', 'sofaShieldVariant', 0.0)
    final static Product OUTLET_PRODUCT_OVERSTOCK = new Product('AUTO_TEST_OUTLET_OVER', 'AUTO_TEST_OVER_VARIANT', 500.0, 'OVERSTOCK')
    final static Product OUTLET_PRODUCT_QUICKSTOCK = new Product('AUTO_TEST_OUTLET_QUICK', 'AUTO_TEST_QUICK_VARIANT', 500.0, 'QUICK')
    final static Product OUTLET_PRODUCT_EX_DISPLAY = new Product('AUTO_TEST_OUTLET_EX_DISPLAY', 'AUTO_TEST_EX_DISPLAY_VARIANT', 500.0, 'EX_DISPLAY')
    final static Product OUTLET_PRODUCT_REFURBISHED_A = new Product('AUTO_TEST_OUTLET_REFURB_A', 'AUTO_TEST_EX_REFURB_A_VARIANT', 500.0, 'REFURBISHED_GRADE_A')
    final static Product OUTLET_PRODUCT_REFURBISHED_B = new Product('AUTO_TEST_OUTLET_REFURB_B', 'AUTO_TEST_EX_REFURB_B_VARIANT', 500.0, 'REFURBISHED_GRADE_B')
    final static Product OCCASIONAL_PRODUCT = new Product('Auto_Tacit_Occasional', 'Auto_Tacit_Occasional_Variant', 500.0)
}
