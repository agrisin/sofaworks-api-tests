package data.constants

import data.dto.Category

import static data.constants.SuperCategories.OCCASIONAL_CATEGORY

class Categories
{
    final static Category OCCASIONAL_VARIANT_CATEGORY = new Category('AUTO_CATEGORY_OCCASIONAL', 'AUTO_CATEGORY_OCCASIONAL_BLACK', OCCASIONAL_CATEGORY)
}
