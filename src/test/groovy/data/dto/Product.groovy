package data.dto

class Product
{
    String base
    String variant
    BigDecimal price
    String stockType

    Product(String base, String variant, BigDecimal price, String stockType = '')
    {
        this.base = base
        this.variant = variant
        this.price = price
        this.stockType = stockType
    }
}
