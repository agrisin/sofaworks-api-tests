package data.dto

class Category
{
    String variant
    String variantValue
    String superCategory

    Category(String variant, String variantValue, String superCategory)
    {
        this.variant = variant
        this.variantValue = variantValue
        this.superCategory = superCategory
    }
}
