package data.sample

import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.CatalogVersions.CATALOG_VERSION_ONLINE
import static data.constants.CatalogVersions.CATALOG_VERSION_STAGED
import static data.constants.Products.OCCASIONAL_PRODUCT
import static data.constants.Products.OUTLET_PRODUCT_EX_DISPLAY
import static data.constants.Products.OUTLET_PRODUCT_OVERSTOCK
import static data.constants.Products.OUTLET_PRODUCT_QUICKSTOCK
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_A
import static data.constants.Products.OUTLET_PRODUCT_REFURBISHED_B
import static data.constants.Products.SECOND_PRODUCT
import static data.constants.Products.SIMPLE_PRODUCT
import static data.constants.Products.UPHOLSTERY_FOOTSTOOL_PRODUCT
import static data.constants.SuperCategories.EX_DISPLAY_CATEGORY
import static data.constants.SuperCategories.FOOTSTOOL_CATEGORY
import static data.constants.SuperCategories.OVERSTOCK_CATEGORY
import static data.constants.SuperCategories.QUICKSTOCK_CATEGORY
import static data.constants.SuperCategories.REFURBISHED_CATEGORY
import static data.constants.SuperCategories.UPHOLSTERY_CATEGORY

class ProductImport implements WithCustomerApi, WithProductApi
{
    void execute()
    {
        addProduct(SIMPLE_PRODUCT)
        addProduct(SECOND_PRODUCT)

        addProduct(UPHOLSTERY_FOOTSTOOL_PRODUCT)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, FOOTSTOOL_CATEGORY)
        addProductCategoryRelation(UPHOLSTERY_FOOTSTOOL_PRODUCT.base, UPHOLSTERY_CATEGORY)

        addOccasionalProduct(OCCASIONAL_PRODUCT, CATALOG_VERSION_STAGED)
        addOccasionalProduct(OCCASIONAL_PRODUCT, CATALOG_VERSION_ONLINE)

        addOutletProduct(OUTLET_PRODUCT_OVERSTOCK, OVERSTOCK_CATEGORY)
        addOutletProduct(OUTLET_PRODUCT_QUICKSTOCK, QUICKSTOCK_CATEGORY)
        addOutletProduct(OUTLET_PRODUCT_EX_DISPLAY, EX_DISPLAY_CATEGORY)
        addOutletProduct(OUTLET_PRODUCT_REFURBISHED_A, REFURBISHED_CATEGORY)
        addOutletProduct(OUTLET_PRODUCT_REFURBISHED_B, REFURBISHED_CATEGORY)
    }
}
