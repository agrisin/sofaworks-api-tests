package data.sample

import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Credentials.CUSTOMER_MANAGER
import static data.constants.Credentials.MERCH_MANAGER
import static data.constants.Credentials.PRICING_MANAGER
import static data.constants.Credentials.SOFOLOGIST_USER
import static data.constants.UserGroups.CUSTOMER_MANAGER_GROUP
import static data.constants.UserGroups.MERCH_USER_GROUP
import static data.constants.UserGroups.PRICING_USER_GROUP

class UserImport implements WithCustomerApi, WithProductApi
{
    void execute()
    {
        addCustomer(SOFOLOGIST_USER)
        addEmployee(CUSTOMER_MANAGER, CUSTOMER_MANAGER_GROUP)
        addEmployee(PRICING_MANAGER, PRICING_USER_GROUP)
        addEmployee(MERCH_MANAGER, MERCH_USER_GROUP)
    }
}
