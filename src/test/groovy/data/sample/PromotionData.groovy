package data.sample

import traits.WithCustomerApi
import traits.WithProductApi

import static data.constants.Promotions.ANONYMOUS_VOUCHER_CODE
import static data.constants.Promotions.ANONYMOUS_VOUCHER_NAME
import static data.constants.Promotions.ANONYMOUS_VOUCHER_VALUE
import static data.constants.Promotions.DISCOUNT_CODE
import static data.constants.Promotions.DISCOUNT_VALUE
import static data.constants.Promotions.MULTI_VALUE_VOUCHER_CODE
import static data.constants.Promotions.MULTI_VALUE_VOUCHER_VALUE
import static data.constants.Promotions.PROMO_CODE

class PromotionData implements WithCustomerApi, WithProductApi
{
    void execute()
    {
        addVoucher(ANONYMOUS_VOUCHER_CODE, ANONYMOUS_VOUCHER_NAME, ANONYMOUS_VOUCHER_VALUE)
        addDiscount(DISCOUNT_CODE, DISCOUNT_VALUE)
        addMultivalueSerialVoucher(MULTI_VALUE_VOUCHER_CODE, MULTI_VALUE_VOUCHER_VALUE)
    }
}
