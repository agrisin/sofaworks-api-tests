package data.environment

import static data.constants.Environments.MASTER
import static data.constants.Environments.QA
import static java.lang.System.getProperty

class Environment
{
    static final NAME = getProperty('config.env', MASTER)
    private static final CONFIG_RESOURCE = 'config/environment/environments.groovy'
    private static final CONFIG = new ConfigSlurper(NAME).parse(new File(CONFIG_RESOURCE).toURI().toURL())
    private static final INSTANCE = new Environment(CONFIG)

    String baseUrl
    String securedBaseUrl
    String adminUrl
    String solrUrl
    String ssh
    String omsUrl
    String logisticsUrl

    static getInstance()
    {
        INSTANCE
    }
}
