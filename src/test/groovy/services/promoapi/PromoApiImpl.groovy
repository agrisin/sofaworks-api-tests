package services.promoapi

import httphandler.AppResponse
import traits.WithApiHelper

class PromoApiImpl implements WithApiHelper
{
    String VOUCHER_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/vouchers/'

    AppResponse generateMultiValueSerialVoucher(String code, String uid)
    {
        Map params = [
                uid: uid
        ]

        getRequest(STORE, "${VOUCHER_API_BASE_PATH}/${code}/generate", params)
    }
}
