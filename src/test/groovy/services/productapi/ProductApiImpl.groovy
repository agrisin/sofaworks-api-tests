package services.productapi

import httphandler.AppResponse
import traits.WithApiHelper

import static org.apache.http.HttpStatus.SC_OK

class ProductApiImpl implements WithApiHelper
{
    String UPDATE_OCCASIONAL_PRODUCTS_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/occasional'
    String OCCASIONAL_PRODUCTS_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/occasionals'
    String PRODUCT_INFO_BASE_PATH = '/pws/rest/catalogs/swkProductCatalog/catalogversions/'
    String PRODUCT_SYNC_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/catalog/synchronize/products'

    AppResponse getProductInfo(String productCode, String catalogVersion)
    {
        getRequestWithBasiAuth(ADMIN, "${PRODUCT_INFO_BASE_PATH}/${catalogVersion}/products/${productCode}")
    }

    void updateProductInfo(String productCode, String catalogVersion, String payload)
    {
        def response = putRequestWithBasiAuth(ADMIN, "${PRODUCT_INFO_BASE_PATH}/${catalogVersion}/products/${productCode}", payload)

        assert response.httpStatus == SC_OK
    }

    void syncProduct(String payload, String accessToken)
    {
        def response = postRequestWithAuth(ADMIN, PRODUCT_SYNC_BASE_PATH, accessToken, [:], payload)

        assert response.httpStatus == SC_OK
    }

    AppResponse getOccasionalProducts(String rangeName, String accessToken)
    {
        getRequestWithAuth(SECURED, "${OCCASIONAL_PRODUCTS_BASE_PATH}/${rangeName}", accessToken)
    }

    void occasionalProductsFieldsUpdate(String accessToken, String payload)
    {
        def response = patchRequestWithAuth(SECURED, UPDATE_OCCASIONAL_PRODUCTS_BASE_PATH, accessToken, [:], payload)
        assert response.httpStatus == SC_OK
    }
}
