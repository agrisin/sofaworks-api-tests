package services.orderapi

import httphandler.AppResponse
import traits.WithApiHelper

class OrderApiImpl implements WithApiHelper
{
    String ORDER_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/users/'
    String GET_ORDER_BASE_PATH = '/pws/rest/orders/'

    AppResponse place(String uid, String cartGuid, String channel, String accessToken, String payload)
    {
        Map params = [
                cartId : cartGuid,
                fields : 'DEFAULT',
                channel: channel
        ]

        def response = postRequestWithAuth(SECURED, "${ORDER_BASE_PATH}/${uid}/orders", accessToken, params, payload)
        checkResponse(response)
        response
    }

    AppResponse amend(String uid, String cartGuid, String accessToken)
    {
        postRequestWithAuth(SECURED, "${ORDER_BASE_PATH}/${uid}/orders/${cartGuid}/amend", accessToken)
    }

    AppResponse details(String orderId)
    {
        getRequestWithBasiAuth(ADMIN, "${GET_ORDER_BASE_PATH}/${orderId}")
    }
}
