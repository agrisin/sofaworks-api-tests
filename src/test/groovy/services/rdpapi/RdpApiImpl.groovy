package services.rdpapi

import httphandler.AppResponse
import traits.WithApiHelper

class RdpApiImpl implements WithApiHelper
{
    String RDP_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/range/'

    AppResponse search(String rangeName)
    {
        Map params = [
                selectedColour: ''
        ]

        getRequest(STORE, "${RDP_API_BASE_PATH}${rangeName}", params)
    }

    AppResponse searchAuthorized(String rangeName, String accessToken)
    {
        Map params = [
                selectedColour: ''
        ]
        def response = getRequestWithAuth(SECURED, "${RDP_API_BASE_PATH}/${rangeName}", accessToken, params)
        checkResponse(response)
        response
    }

    AppResponse searchProducts(String rangeName)
    {
        getRequest(STORE, "${RDP_API_BASE_PATH}${rangeName}/products")
    }
}
