package services.searchapi

import httphandler.AppResponse
import traits.WithApiHelper

class SearchApiImpl implements WithApiHelper
{
    String SEARCH_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/products/search'
    String SEARCH_USERS_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/users'

    AppResponse search()
    {
        Map params = [
                query      : ':nameSortAsc::',
                currentPage: '0',
                pageSize   : '100',
                fields     : 'DEFAULT'
        ]

        getRequest(STORE, SEARCH_API_BASE_PATH, params)
    }

    AppResponse search(String searchTerm)
    {
        Map params = [
                currentPage: '0',
                pageSize   : '100',
                fields     : 'DEFAULT',
                query      : searchTerm
        ]

        getRequest(STORE, SEARCH_API_BASE_PATH, params)
    }

    AppResponse searchUser(String queryParam, String accessToken)
    {
        Map params = [
                query: queryParam
        ]

        getRequestWithAuth(SECURED, SEARCH_USERS_BASE_PATH, accessToken, params)
    }
}
