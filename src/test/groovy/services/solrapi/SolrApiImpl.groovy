package services.solrapi

import httphandler.AppResponse
import traits.WithApiHelper

class SolrApiImpl implements WithApiHelper
{
    String SOLR_PRODUCT_SEARCH_BASE_PATH = '/master_swk_Variant/select'
    String SOLR_RANGE_SEARCH_BASE_PATH = '/master_swk_SupplierRange/select'

    AppResponse search(String searchTerm)
    {
        String query = "code_text:${searchTerm}"
        Map params = [
                q : query,
                wt: 'json'
        ]

        getRequest(SOLR, SOLR_PRODUCT_SEARCH_BASE_PATH, params)
    }

    AppResponse searchRange(String query, String filterQuery = '')
    {
        Map params = [
                q   : query,
                fq  : filterQuery,
                wt  : 'json',
                rows: '100'
        ]

        getRequest(SOLR, SOLR_RANGE_SEARCH_BASE_PATH, params)
    }
}
