package services.financeapi

import httphandler.AppResponse
import traits.WithApiHelper

class FinanceApiImpl implements WithApiHelper
{
    AppResponse applyFinance(String uid, String cartGuid, String submitValue, String payload, String accessToken)
    {
        String path = "/swkcommercewebservices/v2/cmsSofaworksSite/users/${uid}/carts/${cartGuid}/finance?submit=${submitValue}"
        def response = putRequestWithAuth(SECURED, path, accessToken, [:], payload)
        checkResponse(response)

        response
    }

    AppResponse getFinance(String uid, String cartGuid, String accessToken)
    {
        String path = "/swkcommercewebservices/v2/cmsSofaworksSite/users/${uid}/${cartGuid}/finance"
        getRequestWithAuth(SECURED, path, accessToken)
    }

    AppResponse getFromFinanceProvider(String uid, String cartGuid, String accessToken)
    {
        Map params = [
                fields: 'BASIC'
        ]
        String path = "/swkcommercewebservices/v2/cmsSofaworksSite/users/${uid}/${cartGuid}/finance"
        getRequestWithAuth(SECURED, path, accessToken, params)
    }
}
