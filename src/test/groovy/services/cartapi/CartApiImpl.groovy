package services.cartapi

import groovy.json.JsonSlurper
import httphandler.AppResponse
import traits.WithApiHelper

import static org.apache.http.HttpStatus.SC_CREATED
import static org.apache.http.HttpStatus.SC_OK

class CartApiImpl implements WithApiHelper
{
    String CART_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/users/'

    String create(String uid, String accessToken)
    {
        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts", accessToken)
        assert response.httpStatus == SC_CREATED

        new JsonSlurper().parseText(response.body).guid
    }

    String createAnonymous(String uid)
    {
        def response = postRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts")
        assert response.httpStatus == SC_CREATED

        new JsonSlurper().parseText(response.body).guid
    }

    AppResponse details(String uid, String cartGuid, String accessToken)
    {
        getRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}", accessToken)
    }

    AppResponse detailsAnonymous(String uid, String cartGuid)
    {
        getRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}")
    }

    void setDeliveryPostalCode(String uid, String cartGuid, String postalCode, String accessToken)
    {
        def params = [
                postalCode: postalCode
        ]

        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/postalCode", accessToken, params)
        checkResponse(response)
    }

    void setDeliveryPostalCodeAnonymous(String uid, String cartGuid, String postalCode)
    {
        def params = [
                postalCode: postalCode
        ]

        def response = postRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/postalCode", params)
        checkResponse(response)
    }

    AppResponse review(String uid, String cartGuid, String accessToken)
    {
        def params = [
                fields: 'FULL'
        ]

        getRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}", accessToken, params)
    }

    void addEntry(String uid, String cartGuid, String itemId, String accessToken)
    {
        def params = [
                code: itemId
        ]

        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries", accessToken, params)
        String statusCode = new JsonSlurper().parseText(response.body).statusCode

        checkResponse(response)
        assert statusCode == 'success'
    }

    AppResponse addEntryAnonymous(String uid, String cartGuid, String itemId)
    {
        def params = [
                code: itemId
        ]

        postRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries", params)
    }

    AppResponse changeEntryQuantity(String uid, String cartGuid, int quantity, String accessToken)
    {
        def params = [
                qty: quantity.toString()
        ]

        patchRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries/0", accessToken, params)
    }

    AppResponse getAddonEntries(String uid, String cartGuid, String addonItemId, String accessToken)
    {
        getRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/addons/${addonItemId}", accessToken)
    }

    AppResponse addOutletEntry(String uid, String cartGuid, String itemId, String stockType, String accessToken, BigDecimal quantity = 1)
    {
        def params = [
                code     : itemId,
                qty      : quantity.toString(),
                stockType: stockType
        ]

        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries", accessToken, params)
        String statusCode = new JsonSlurper().parseText(response.body).statusCode

        checkResponse(response)
        assert statusCode == 'success'

        response
    }

    void removeEntry(String uid, String cartGuid, int entryId, String accessToken)
    {
        def response = deleteRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries/${entryId}", accessToken)
        assert response.httpStatus == SC_OK
    }

    void removeCart(String uid, String cartGuid, String accessToken)
    {
        def response = deleteRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}", accessToken)
        assert response.httpStatus == SC_OK
    }

    void addDeliveryAddress(String uid, String cartGuid, String addressId, String accessToken)
    {
        def params = [
                addressId: addressId
        ]

        def response = putRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/addresses/delivery", accessToken, params)

        checkResponse(response)
    }

    void setDeliveryOption(String uid, String cartGuid, String deliveryOption, String accessToken)
    {
        def params = [
                code: deliveryOption
        ]

        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/deliveryoptions", accessToken, params)
        checkResponse(response)
    }

    void setDeliveryOptionAnonymous(String uid, String cartGuid, String deliveryOption)
    {
        def params = [
                code: deliveryOption
        ]

        def response = postRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/deliveryoptions", params)
        assert response.httpStatus == SC_OK
    }

    AppResponse confirmReservation(String uid, String cartGuid, String accessToken)
    {
        postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/confirmReservation", accessToken)
    }

    AppResponse addPickUpLocation(String uid, String cartGuid, String accessToken, String location = 'BATHGATE')
    {
        Map params = [
                pickupStore: location
        ]

        patchRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/entries/0", accessToken, params)
    }

    AppResponse deliveryMode(String uid, String cartGuid, String accessToken)
    {
        getRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/deliverymode", accessToken)
    }

    void applyVoucher(String uid, String cartGuid, String voucherId, String accessToken)
    {
        def params = [
                voucherId: voucherId
        ]

        def response = postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/vouchers", accessToken, params)

        checkResponse(response)
    }

    void applyVoucherAnonymous(String uid, String cartGuid, String voucherId)
    {
        def params = [
                voucherId: voucherId
        ]

        def response = postRequest(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/vouchers", params)

        checkResponse(response)
    }

    void applyOnetimeDiscount(String uid, String cartGuid, String accessToken, String payload)
    {
        String path = "${CART_API_BASE_PATH}/${uid}/${cartGuid}/discount"

        def response = postRequestWithAuth(SECURED, path, accessToken, [:], payload)

        checkResponse(response)
    }

    AppResponse applyEcaDate(String uid, String cartGuid, String ecaDate, String accessToken)
    {
        def params = [
                date: ecaDate
        ]

        postRequestWithAuth(SECURED, "${CART_API_BASE_PATH}/${uid}/carts/${cartGuid}/eca", accessToken, params)
    }
}
