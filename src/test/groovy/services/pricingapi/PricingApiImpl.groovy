package services.pricingapi

import httphandler.AppResponse
import traits.WithApiHelper

class PricingApiImpl implements WithApiHelper
{
    String PRICING_RANGE_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/pricing/range/'
    String PRICING_RANGE_OCCASIONAL_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/pricing/occasional/range/'
    String PRICING_UPDATE_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/pricing/update'

    AppResponse priceForRange(String rangeName, String accessToken)
    {
        Map params = [
                fields  : 'FULL',
                pageSize: '540'
        ]

        getRequestWithAuth(STORE, "${PRICING_RANGE_BASE_PATH}/${rangeName}", accessToken, params)
    }

    AppResponse priceForOccasionalRange(String rangeName, String accessToken)
    {
        getRequestWithAuth(STORE, "${PRICING_RANGE_OCCASIONAL_BASE_PATH}/${rangeName}", accessToken)
    }

    void priceUpdate(String accessToken, String payload)
    {
        def response = postRequestWithAuth(STORE, PRICING_UPDATE_BASE_PATH, accessToken, [:], payload)

        checkResponse(response)
    }
}
