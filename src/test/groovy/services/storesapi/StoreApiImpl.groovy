package services.storesapi

import httphandler.AppResponse
import traits.WithApiHelper

class StoreApiImpl implements WithApiHelper
{
    String CURRENT_STORE_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/stores/current'

    AppResponse getCurrent(String accessToken)
    {
        getRequestWithAuth(STORE, "${CURRENT_STORE_BASE_PATH}", accessToken)
    }
}
