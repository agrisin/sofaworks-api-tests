package services.curationapi

import httphandler.AppResponse
import traits.WithApiHelper

class CurationApiImpl implements WithApiHelper
{
    String CURATION_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/category/'

    AppResponse getList(String category, String accessToken)
    {
        getRequestWithAuth(STORE, "${CURATION_BASE_PATH}/${category}/curation", accessToken)
    }

    AppResponse putList(String category, String accessToken, String payload)
    {
        putRequestWithAuth(SECURED, "${CURATION_BASE_PATH}/${category}/curation", accessToken, [:], payload)
    }

    AppResponse deleteList(String category, String accessToken)
    {
        deleteRequestWithAuth(STORE, "${CURATION_BASE_PATH}/${category}/curation", accessToken)
    }
}
