package services.logisticsapi

import httphandler.AppResponse
import traits.WithApiHelper

class LogisticsApiImpl implements WithApiHelper
{
    String BY_POST_CODE_BASE_PATH = '/postcode'

    AppResponse getDelivery(String postCode)
    {
        postCode = postCode.replace(' ', '%20')
        getRequest(LOGISTICS, "${BY_POST_CODE_BASE_PATH}/${postCode}")
    }
}
