package services.categoryapi

import httphandler.AppResponse
import traits.WithApiHelper

class CategoryApiImpl implements WithApiHelper
{
    String CATEGORIES_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/categories/'
    String OCCASIONAL_CATEGORIES_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/occasional/categories'
    String SWATCHES_CATEGORIES_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/swatches/occasional'
    String OUTLET_CATEGORY_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/categories/outlet/'
    String SUPER_CATEGORY_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/categories/swkProductCatalog/Online/category-hierarchy/'
    String CATEGORY_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/category/'

    AppResponse search(String name)
    {
        getRequest(STORE, "${CATEGORIES_BASE_PATH}${name}")
    }

    AppResponse outlet(String outletCategory)
    {
        getRequest(STORE, "${OUTLET_CATEGORY_BASE_PATH}${outletCategory}")
    }

    AppResponse outletProducts(String rangeName, String postalCode, String outletCategory)
    {
        Map params = [
                range     : rangeName,
                postalCode: postalCode
        ]

        getRequest(STORE, "${OUTLET_CATEGORY_BASE_PATH}${outletCategory}", params)
    }

    AppResponse superCategory(String category)
    {
        getRequest(STORE, "${SUPER_CATEGORY_BASE_PATH}/${category}")
    }

    AppResponse occasional(String accessToken)
    {
        getRequestWithAuth(STORE, "${OCCASIONAL_CATEGORIES_BASE_PATH}", accessToken)
    }

    AppResponse swatches()
    {
        getRequest(STORE, "${SWATCHES_CATEGORIES_BASE_PATH}")
    }

    AppResponse addProductToCategory(String product, String category, String accessToken)
    {
        String payload = "{\"products\": [\"${product}\"]}"

        putRequestWithAuth(STORE, "${CATEGORY_BASE_PATH}/${category}/products", accessToken, [:], payload)
    }
}
