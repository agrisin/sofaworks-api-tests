package services.quicksearch

import httphandler.AppResponse
import traits.WithApiHelper

class QuickSearchApiImpl implements WithApiHelper
{
    String QUICK_SEARCH_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/products/quickSearch'

    AppResponse search()
    {
        Map params = [
                query: ':nameSortAsc::'
        ]

        getRequest(STORE, QUICK_SEARCH_API_BASE_PATH, params)
    }

    AppResponse search(String searchTerm)
    {
        Map params = [
                currentPage: '0',
                pageSize   : '100',
                fields     : 'DEFAULT',
                query      : searchTerm
        ]

        getRequest(STORE, QUICK_SEARCH_API_BASE_PATH, params)
    }
}
