package services

import services.accountapi.AccountApiImpl
import services.adminapi.AdminApiImpl
import services.cartapi.CartApiImpl
import services.categoryapi.CategoryApiImpl
import services.curationapi.CurationApiImpl
import services.financeapi.FinanceApiImpl
import services.logisticsapi.LogisticsApiImpl
import services.omsapi.OmsApiImpl
import services.orderapi.OrderApiImpl
import services.pricingapi.PricingApiImpl
import services.productapi.ProductApiImpl
import services.promoapi.PromoApiImpl
import services.quicksearch.QuickSearchApiImpl
import services.rdpapi.RdpApiImpl
import services.searchapi.SearchApiImpl
import services.solrapi.SolrApiImpl
import services.storesapi.StoreApiImpl

class ApiClient
{
    static getAdminApi()
    {
        new AdminApiImpl()
    }

    static getLogisticsApi()
    {
        new LogisticsApiImpl()
    }

    static getAccountApi()
    {
        new AccountApiImpl()
    }

    static getCartApi()
    {
        new CartApiImpl()
    }

    static getCurationApi()
    {
        new CurationApiImpl()
    }

    static getFinanceApi()
    {
        new FinanceApiImpl()
    }

    static getStoreApi()
    {
        new StoreApiImpl()
    }

    static getPromoApi()
    {
        new PromoApiImpl()
    }

    static getSearchApi()
    {
        new SearchApiImpl()
    }

    static getQuickSearchApi()
    {
        new QuickSearchApiImpl()
    }

    static getOmsApi()
    {
        new OmsApiImpl()
    }

    static getOrderApi()
    {
        new OrderApiImpl()
    }

    static getPricingApi()
    {
        new PricingApiImpl()
    }

    static getProductApi()
    {
        new ProductApiImpl()
    }

    static getRdpApi()
    {
        new RdpApiImpl()
    }

    static getCategoryApi()
    {
        new CategoryApiImpl()
    }

    static getSolrApi()
    {
        new SolrApiImpl()
    }
}
