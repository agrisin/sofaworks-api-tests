package services.adminapi

import adminconsole.FlexibleSearchRunner
import adminconsole.ImpexRunner
import adminconsole.ScriptRunner
import groovy.json.JsonSlurper
import httphandler.AppResponse
import traits.WithAdminApi

class AdminApiImpl implements WithAdminApi
{
    def jsonSlurper = new JsonSlurper()

    final SCRIPT_TEMPLATE = 'runCronjob.groovy'
    final CRON_JOBS_BASE_PATH = '/pws/rest/cronjobs'

    AppResponse flexSearchFromString(String fsQuery)
    {
        FlexibleSearchRunner fsRunner = new FlexibleSearchRunner()
        fsRunner.execute(fsQuery)
    }

    String flexSearch(String fileName, Map query = [:])
    {
        FlexibleSearchRunner fsRunner = new FlexibleSearchRunner()
        List results = fsRunner.fromTemplate(fileName, query)
        Collections.shuffle(results)
        results.size() == 0 ? '' : results[0]
    }

    List flexSearchMultipleResult(String fileName, Map query = [:])
    {
        FlexibleSearchRunner fsRunner = new FlexibleSearchRunner()
        fsRunner.fromTemplate(fileName, query)
    }

    void impexImport(String fileName, Map query)
    {
        ImpexRunner impexRunner = new ImpexRunner()
        impexRunner.fromTemplate(fileName, query)
    }

    AppResponse executeCronJob(String cronJobName)
    {
        Map script_map = [cronjob: cronJobName]
        ScriptRunner scriptRunner = new ScriptRunner()
        scriptRunner.fromTemplate(SCRIPT_TEMPLATE, script_map)
    }

    String runScript(String script)
    {
        ScriptRunner scriptRunner = new ScriptRunner()
        def response = scriptRunner.execute(script)
        jsonSlurper.parseText(response.body).executionResult
    }

    AppResponse getCronJobsList()
    {
        getRequestWithBasiAuth(ADMIN, CRON_JOBS_BASE_PATH)
    }
}
