package services.omsapi

import data.constants.Payloads
import httphandler.AppResponse
import traits.WithApiHelper

import static org.apache.http.HttpStatus.SC_OK

class OmsApiImpl implements WithApiHelper
{
    String OMS_FINANCE_SERVICE = '/api/queues/Finance/Finance.Application.Service.ISubmitApplicationCommand_log/get'
    String OMS_CREATE_ORDER_SERVICE = '/api/queues/Sales/ha.ExponeaHybris.CreateOrder/get'
    String PUBLISH_BASE_PATH = '/api/exchanges/Finance/amq.default/publish'

    void publish(String cartGuid)
    {
        String encodedFinanceProviderPayload = preparePayload(Payloads.FINANCE_PROVIDER_MESSAGE_PAYLOAD, [
                cartGuid: cartGuid
        ])
                .getBytes().encodeBase64().toString()
        String payload = preparePayload(Payloads.PUBLISH_FINANCE_PAYLOAD, [
                encodedPayLoad: encodedFinanceProviderPayload
        ])
        def response = postOmsRequest(OMS, PUBLISH_BASE_PATH, payload)

        assert response.httpStatus == SC_OK
    }

    void update(String cartGuid, int code, String statusDescription)
    {
        String payload = preparePayload(Payloads.UPDATE_FINANCE_PAYLOAD, [
                cartGuid         : cartGuid,
                code             : code,
                statusDescription: statusDescription
        ])
        def response = postOmsRequest(OMS, PUBLISH_BASE_PATH, payload)

        assert response.httpStatus == SC_OK
    }

    AppResponse getQueue()
    {
        String payload = preparePayload(Payloads.GET_FINANCE_QUEUE_PAYLOAD)
        postOmsRequest(OMS, OMS_FINANCE_SERVICE, payload)
    }

    AppResponse getCreateOrderQueue()
    {
        String payload = preparePayload(Payloads.GET_CREATE_ORDER_QUEUE_PAYLOAD)
        postOmsRequest(OMS, OMS_CREATE_ORDER_SERVICE, payload)
    }
}
