package services.accountapi

import httphandler.AppResponse
import traits.WithApiHelper

import static org.apache.http.entity.ContentType.APPLICATION_XML

class AccountApiImpl implements WithApiHelper
{
    String ACCOUNT_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/users'
    String ACCOUNT_LOGIN_API_BASE_PATH = '/swkcommercewebservices/oauth/token'
    String RECENTLY_VIEWED_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/range/recent'
    String USER_REVIEW_API_BASE_PATH = '/swkcommercewebservices/v2/cmsSofaworksSite/range/AKIRA/reviews'
    String UPDATE_EMAIL_BASE_PATH = '/swkbackofficewebservices/v2/cmsSofaworksSite/customers/updateEmail'

    AppResponse register(String uid, String password)
    {
        Map params = [
                uid     : uid,
                password: password
        ]

        postRequest(SECURED, ACCOUNT_API_BASE_PATH, params)
    }

    AppResponse login(String uid, String password)
    {
        Map params = [
                client_id     : 'commerce_app',
                client_secret : 'commerce_app_secret',
                grant_type    : 'password',
                username      : uid,
                password      : password,
                pointOfService: 'BATHGATE'
        ]

        def response = postRequest(SECURED, ACCOUNT_LOGIN_API_BASE_PATH, params)

        response
    }

    AppResponse loginByEmailFB(String uid, String accessToken)
    {
        postRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}/loginWithEmail", accessToken)
    }

    AppResponse logout(String uid, String accessToken)
    {
        postRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}/logout", accessToken)
    }

    AppResponse myProfile(String uid, String accessToken)
    {
        getRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}", accessToken)
    }

    AppResponse recentlyViewed(String accessToken)
    {
        getRequestWithAuth(SECURED, RECENTLY_VIEWED_API_BASE_PATH, accessToken)
    }

    AppResponse addAddress(String uid, String accessToken, String payload)
    {
        postRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}/addresses", accessToken, [:], payload)
    }

    AppResponse getAddress(String uid, String accessToken)
    {
        getRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}/addresses", accessToken)
    }

    AppResponse deleteAddress(String uid, String addressId, String accessToken)
    {
        deleteRequestWithAuth(SECURED, "${ACCOUNT_API_BASE_PATH}/${uid}/addresses/${addressId}", accessToken)
    }

    AppResponse submitReview(String accessToken, String payload)
    {
        putRequestWithAuth(SECURED, USER_REVIEW_API_BASE_PATH, accessToken, [:], payload, APPLICATION_XML)
    }

    AppResponse getReview(String accessToken)
    {
        getRequestWithAuth(SECURED, USER_REVIEW_API_BASE_PATH, accessToken)
    }

    AppResponse updateEmail(String uid, String newUid, String accessToken)
    {
        Map params = [
                uid     : uid,
                newEmail: newUid
        ]

        postRequestWithAuth(SECURED, UPDATE_EMAIL_BASE_PATH, accessToken, params)
    }
}
